package prvnimilion.vutindex.ui;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Queue;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by stand on 13.08.2017.
 */

public class ObservableLifeCycleManager {
  volatile boolean isPaused = false;
  private HashMap<String, SingleInfo> singles;
  private HashMap<String, ObserverInfo> observables;

  public ObservableLifeCycleManager() {
    singles = new HashMap<>();
    observables = new HashMap<>();
  }

  public <T> void addObservableAndStartObserve(String id, Observable observable, Observer<T> observer, boolean cachePaused, Scheduler scheduler) {
    ObserverInfo oi = new ObserverInfo();
    oi.id = id;
    oi.observable = observable;
    oi.observer = observer;
    oi.isCached = cachePaused;
    oi.wasCompleted = false;
    oi.pausedError = null;
    oi.pausedMessages = new ArrayDeque();
    oi.scheduler = scheduler;

    observables.put(id, oi);

    startObserve(oi);
  }

  public <E extends SingleObserver> void addSingleAndStartObserve(String id, Single source, E observer) {
    SingleInfo si = new SingleInfo();
    si.id = id;
    si.observer = observer;
    si.source = source;

    singles.put(id, si);

    startObserve(si);
  }

  private void startObserve(final ObserverInfo oi) {
    oi.disposableListener = new DisposableObserver() {
      @Override
      public void onNext(Object value) {
        if (isPaused) {
          if (oi.isCached) {
            oi.pausedMessages.add(value);
          } else {
            //do nothing, if context is paused and observer do not use cache we discard the messages
          }
        } else {
          flushCache(oi);
          oi.observer.onNext(value);
        }
      }

      @Override
      public void onError(Throwable e) {
        if (isPaused) {
          if (oi.isCached) {
            oi.pausedError = e;
          } else {
            //do nothing, if context is paused and observer do not use cache we discard the messages
          }
        } else {
          flushCache(oi);
          oi.observer.onError(e);
          removeObservable(oi);
        }
      }

      @Override
      public void onComplete() {
        if (isPaused) {
          if (oi.isCached) {
            oi.wasCompleted = true;
          } else {
            //do nothing, if context is paused and observer do not use cache we discard the messages
          }
        } else {
          flushCache(oi);
          oi.observer.onComplete();
          removeObservable(oi);
        }
      }
    };

    oi.observable.observeOn(oi.scheduler).subscribe(oi.disposableListener);

  }

  public void startObserve(final SingleInfo si) {
    si.disposableListener = new DisposableSingleObserver() {
      @Override
      public void onSuccess(Object value) {
        si.observer.onSuccess(value);
        removeObservable(si);
      }

      @Override
      public void onError(Throwable e) {
        si.observer.onError(e);
        removeObservable(si);
      }
    };

    si.source.subscribe(si.disposableListener);

  }

  public void raiseOnResume() {
    if (isPaused) {
      for (String oKey : observables.keySet()) {
        ObserverInfo observableInfo = observables.get(oKey);

        flushCache(observableInfo);

        if (observableInfo.pausedError != null) {
          observableInfo.scheduler.scheduleDirect(() -> observableInfo.observer.onError(observableInfo.pausedError));
        } else if (observableInfo.wasCompleted) {
          observableInfo.scheduler.scheduleDirect(() -> observableInfo.observer.onComplete());
        }
      }

      for (String sKey : singles.keySet()) {
        SingleInfo singleInfo = singles.get(sKey);
        startObserve(singleInfo);
      }

      isPaused = false;
    }
  }

  private void flushCache(ObserverInfo observableInfo) {
    for (Object message : observableInfo.pausedMessages) {
      observableInfo.scheduler.scheduleDirect(() -> observableInfo.observer.onNext(message));
    }
    observableInfo.pausedMessages.clear();
  }

  public void raiseOnPause() {
    isPaused = true;

    for (String sKey : singles.keySet()) {
      SingleInfo singleInfo = singles.get(sKey);
      singleInfo.disposableListener.dispose();
      singleInfo.disposableListener = null;
    }
  }

  public void removeObservable(SingleInfo si) {
    si.disposableListener.dispose();
    si.disposableListener = null;
    singles.remove(si.id);
  }

  public void removeObservable(ObserverInfo oi) {
    oi.disposableListener.dispose();
    oi.disposableListener = null;
    observables.remove(oi.id);
  }

  public interface IObservableLifeCycleManager {
    ObservableLifeCycleManager getObservableLifeCycleManager();
  }

  private class ObserverInfo {
    public String id;
    public Observable observable;
    public Observer observer;
    public boolean isCached;
    public boolean wasCompleted;
    public Throwable pausedError;
    public Queue pausedMessages;
    public DisposableObserver disposableListener;
    public Scheduler scheduler;
  }

  private class SingleInfo {
    public String id;
    public Single source;
    public SingleObserver observer;
    public DisposableSingleObserver disposableListener;
  }
}
