package prvnimilion.vutindex.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.ActivityMainBinding;
import prvnimilion.vutindex.mvp.presenters.MainPresenter;
import prvnimilion.vutindex.mvp.views.MainActivityMvpView;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;

public class MainActivity extends VutIndexBaseActivity implements MainActivityMvpView {

  @Inject
  public MainPresenter presenter;
  private ActivityMainBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_main, getWindow().getDecorView().findViewById(android.R.id.content), false);
    setContentView(binding.getRoot());
    binding.setPresenter(presenter);
    presenter.attachView(this, savedInstanceState == null);
  }

  public void showToast(String content) {
    Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
  }

  @Override
  public Activity getActivity() {
    return this;
  }

  @Override
  public Context getContext() {
    return getContext();
  }

  @Override
  public View getLayout() {
    return binding.mainLayout;
  }

  @Override
  public ImageView getIcon() {
    return binding.logo;
  }

  @Override
  protected void inject() {
    activityComponent().inject(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    setClickable(true);
    binding.loadingPanel.setVisibility(View.GONE);

    if (!binding.checkBox.isChecked()) {
      binding.loginName.setText("");
      binding.LoginPwd.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }


  @Override
  public void showLoadingPanel(boolean show) {
    if (show) {
      binding.loadingPanel.bringToFront();
      binding.loadingPanel.setVisibility(View.VISIBLE);
    } else {
      binding.loadingPanel.setVisibility(View.GONE);
    }
    setClickable(!show);
  }

  private void setClickable(boolean clickable) {
    if (clickable) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    } else {
      getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    moveTaskToBack(true);
  }
}
