package prvnimilion.vutindex.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import butterknife.ButterKnife;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.ActivityIndexBinding;
import prvnimilion.vutindex.mvp.presenters.IndexPresenter;
import prvnimilion.vutindex.mvp.views.IndexActivityMvpView;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;

public class IndexActivity extends VutIndexBaseActivity implements IndexActivityMvpView {

  @Inject
  public IndexPresenter presenter;
  private ActivityIndexBinding binding;
  private boolean immersiveEnabled = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_index, getWindow().getDecorView().findViewById(android.R.id.content), false);
    setContentView(binding.getRoot());
    binding.setPresenter(presenter);
    presenter.attachView(this, savedInstanceState == null);

    ButterKnife.bind(this);
    setSupportActionBar(findViewById(R.id.index_activity_toolbar));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setTitle(this.getString(R.string.app_name));
    getSupportActionBar().setDisplayShowTitleEnabled(false);

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_activity_index, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    return presenter.onOptionsItemSelected(item);
  }

  public void showToast(String content) {
    Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
  }

  @Override
  public Activity getActivity() {
    return this;
  }

  @Override
  public Context getContext() {
    return getContext();
  }

  @Override
  public TableView getTableView() {
    return binding.indexActivityTableView;
  }

  @Override
  protected void inject() {
    activityComponent().inject(this);
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    if (hasFocus && immersiveEnabled) {
      getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
  }

  @Override
  public void showLoadingPanel(boolean show) {
    if (show) {
      binding.loadingPanel.bringToFront();
      binding.loadingPanel.setVisibility(View.VISIBLE);
    } else {
      binding.loadingPanel.setVisibility(View.GONE);
    }
    setClickable(!show);
  }

  private void setClickable(boolean clickable) {
    if (clickable) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    } else {
      getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
  }

  @Override
  public void onBackPressed() {
    Intent intent = new Intent(this, DashboardActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    this.startActivity(intent);
    this.finish();
  }

  @Override
  public boolean onSupportNavigateUp() {
    Intent intent = new Intent(this, DashboardActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    this.startActivity(intent);
    this.finish();
    return true;
  }

  @Override
  public void toggleImmersive(boolean on) {
    View decorView = getWindow().getDecorView();

    if (!on) {
      // Immersive Mode is on, turn it off.
      decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    } else {
      // Immersive Mode is off, turn it on.
      decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    immersiveEnabled = on;
  }
}
