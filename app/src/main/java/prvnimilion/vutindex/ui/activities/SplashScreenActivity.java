package prvnimilion.vutindex.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.ActivitySplashScreenBinding;
import prvnimilion.vutindex.mvp.presenters.SplashScreenPresenter;
import prvnimilion.vutindex.mvp.views.SplashScreenActivityMvpView;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;

public class SplashScreenActivity extends VutIndexBaseActivity implements SplashScreenActivityMvpView {

  @Inject
  public SplashScreenPresenter presenter;
  private ActivitySplashScreenBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_splash_screen, getWindow().getDecorView().findViewById(android.R.id.content), false);
    setContentView(binding.getRoot());
    binding.setPresenter(presenter);
    presenter.attachView(this, savedInstanceState == null);
  }

  public void showToast(String content) {
    Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
  }

  @Override
  public Activity getActivity() {
    return this;
  }

  @Override
  public View getLayout() {
    return binding.splashLayout;
  }


  @Override
  public Context getContext() {
    return getContext();
  }

  @Override
  protected void inject() {
    activityComponent().inject(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    if (hasFocus) {
      View decorView = getWindow().getDecorView();
      decorView.setSystemUiVisibility(
              View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                      | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
  }

}
