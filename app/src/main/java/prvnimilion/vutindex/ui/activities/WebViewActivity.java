package prvnimilion.vutindex.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import butterknife.ButterKnife;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.ActivityWebviewBinding;
import prvnimilion.vutindex.mvp.presenters.WebViewPresenter;
import prvnimilion.vutindex.mvp.views.WebViewMvpView;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;


public class WebViewActivity extends VutIndexBaseActivity implements WebViewMvpView {
  public final static String EXTRA_WEBVIEW_URL = "WebViewActivity_EXTRA_URL";
  @Inject
  public WebViewPresenter presenter;
  private ActivityWebviewBinding binding;

  public static Intent createStartIntent(Activity caller, String url) {
    Intent startIntent = new Intent(caller, WebViewActivity.class);
    startIntent.putExtra(EXTRA_WEBVIEW_URL, url);
    return startIntent;
  }

  public static String getUrl(Intent intent) {
    return intent.getStringExtra(EXTRA_WEBVIEW_URL);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_webview, getWindow().getDecorView().findViewById(android.R.id.content), false);
    setContentView(binding.getRoot());

    binding.setPresenter(presenter);
    presenter.attachView(this, savedInstanceState == null);

    ButterKnife.bind(this);
    setSupportActionBar(findViewById(R.id.webview_activity_toolbar));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setTitle(this.getString(R.string.app_name));
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_activity_webview, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    return presenter.onOptionsItemSelected(item);
  }

  @Override
  public void inject() {
    activityComponent().inject(this);
  }

  @Override
  public Activity getActivity() {
    return this;
  }

  @Override
  public void showToast(String content) {
    Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
  }

  @Override
  public View getWebView() {
    return findViewById(R.id.activity_webview_wv);
  }

  @Override
  public void showLoadingPanel(boolean show) {
    if (show) {
      binding.loadingPanel.bringToFront();
      binding.loadingPanel.setVisibility(View.VISIBLE);
    } else {
      binding.loadingPanel.setVisibility(View.GONE);
    }
    setClickable(!show);
  }

  private void setClickable(boolean clickable) {
    if (clickable) {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    } else {
      getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
  }

  @Override
  public void onBackPressed() {
    presenter.goBack();
  }

  @Override
  public boolean onSupportNavigateUp() {
    Intent intent = new Intent(this, DashboardActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    this.startActivity(intent);
    this.finish();
    return true;
  }
}
