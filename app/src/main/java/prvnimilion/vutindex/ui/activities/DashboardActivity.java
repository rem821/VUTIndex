package prvnimilion.vutindex.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.ButterKnife;
import hotchemi.android.rate.AppRate;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.ActivityDashboardBinding;
import prvnimilion.vutindex.mvp.presenters.DashboardPresenter;
import prvnimilion.vutindex.mvp.views.DashboardActivityMvpView;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;
import prvnimilion.vutindex.ui.fragments.dialogs.DonationsDialog;

public class DashboardActivity extends VutIndexBaseActivity implements DashboardActivityMvpView {

  @Inject
  public DashboardPresenter presenter;
  private ActivityDashboardBinding binding;
  private DonationsDialog donationsDialog;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_dashboard, getWindow().getDecorView().findViewById(android.R.id.content), false);
    setContentView(binding.getRoot());
    binding.setPresenter(presenter);
    presenter.attachView(this, savedInstanceState == null);

    ButterKnife.bind(this);
    setSupportActionBar(findViewById(R.id.dashboard_activity_toolbar));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setTitle(this.getString(R.string.app_name));

    AppRate.with(this)
            .setInstallDays(0) // default 10, 0 means install day.
            .setLaunchTimes(3) // default 10
            .setRemindInterval(1) // default 1
            .setShowLaterButton(true) // default true
            .setDebug(false) // default false
            .setOnClickButtonListener(which -> Log.d(MainActivity.class.getName(), Integer.toString(which)))
            .monitor();

    AppRate.showRateDialogIfMeetsConditions(this);

  }

  @Override
  protected void onPostCreate(@Nullable Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    presenter.drawerToggle.syncState();
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_activity_dashboard, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (presenter.drawerToggle.onOptionsItemSelected(item)) {
      return true;
    }
    return presenter.onOptionsItemSelected(item);
  }

  public void showToast(String content) {
    Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
  }

  @Override
  public Activity getActivity() {
    return this;
  }

  @Override
  public Context getContext() {
    return getContext();
  }

  @Override
  protected void inject() {
    activityComponent().inject(this);
  }

  @Override
  public NavigationView getNvDrawerMenu() {
    return binding.activityDashboardNvDrawerMenu;
  }

  @Override
  public DrawerLayout getDrawerLayout() {
    return binding.activityDashboardDrawerLayout;
  }

  @Override
  protected void onResume() {
    super.onResume();

    for (int i = 0; i < binding.activityDashboardNvDrawerMenu.getMenu().size(); i++) {
      binding.activityDashboardNvDrawerMenu.getMenu().getItem(i).setChecked(false);
    }
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    moveTaskToBack(true);
  }

  @Override
  public void showDonationsDialog() {
    if (getFragmentManager().findFragmentByTag("donationsDialog") == null) {
      donationsDialog = DonationsDialog.newInstance();
      donationsDialog.show(getSupportFragmentManager(), "donationsDialog");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }
}
