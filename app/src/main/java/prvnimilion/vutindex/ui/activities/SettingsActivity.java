package prvnimilion.vutindex.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.databinding.DataBindingUtil;
import butterknife.ButterKnife;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.ActivitySettingsBinding;
import prvnimilion.vutindex.mvp.presenters.SettingsPresenter;
import prvnimilion.vutindex.mvp.views.SettingsActivityMvpView;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;
import prvnimilion.vutindex.ui.fragments.dialogs.StartupModuleDialog;

public class SettingsActivity extends VutIndexBaseActivity implements SettingsActivityMvpView {

  @Inject
  public SettingsPresenter presenter;
  private ActivitySettingsBinding binding;
  private StartupModuleDialog startupModuleDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_settings, getWindow().getDecorView().findViewById(android.R.id.content), false);
    setContentView(binding.getRoot());
    binding.setPresenter(presenter);
    presenter.attachView(this, savedInstanceState == null);

    ButterKnife.bind(this);
    setSupportActionBar(findViewById(R.id.settings_activity_toolbar));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setTitle(this.getString(R.string.settings));

    binding.seekbar.setThumb(AppCompatResources.getDrawable(getActivity(), R.drawable.ic_school_red_24dp));

  }

  public void showToast(String content) {
    Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
  }

  @Override
  public Activity getActivity() {
    return this;
  }

  @Override
  public Context getContext() {
    return getContext();
  }

  @Override
  public SeekBar getSeekbar() {
    return binding.seekbar;
  }

  @Override
  public TextView getSeekbarCaption() {
    return binding.seekbarCaption;
  }

  @Override
  public void showStartupModuleDialog() {
    if (getFragmentManager().findFragmentByTag("startupModuleDialog") == null) {
      startupModuleDialog = StartupModuleDialog.newInstance();
      startupModuleDialog.show(getSupportFragmentManager(), "startupModuleDialog");
    }
  }

  @Override
  protected void inject() {
    activityComponent().inject(this);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    presenter.onBackPressed();
  }
}
