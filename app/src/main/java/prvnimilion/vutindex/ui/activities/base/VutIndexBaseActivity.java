package prvnimilion.vutindex.ui.activities.base;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.LongSparseArray;
import android.view.View;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.concurrent.atomic.AtomicLong;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import prvnimilion.vutindex.VutIndexApplication;
import prvnimilion.vutindex.di.components.ActivityComponent;
import prvnimilion.vutindex.di.components.ConfigPersistentComponent;
import prvnimilion.vutindex.di.components.DaggerConfigPersistentComponent;
import prvnimilion.vutindex.di.modul.ActivityModule;
import prvnimilion.vutindex.services.IndexService;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;


public abstract class VutIndexBaseActivity extends AppCompatActivity implements ObservableLifeCycleManager.IObservableLifeCycleManager {
  private static final String EXTRA_CAPTION = "VutIndexBaseActivity_EXTRA_CAPTION";
  private static final String KEY_ACTIVITY_ID = "KEY_ACTIVITY_ID";
  private static final AtomicLong NEXT_ID = new AtomicLong(0);
  private static final LongSparseArray<ConfigPersistentComponent> sComponentsArray = new LongSparseArray<>();
  private static final LongSparseArray<ObservableLifeCycleManager> lcManagers = new LongSparseArray<>();
  public static boolean isAppWentToBg = false;
  public static boolean isWindowFocused = false;
  public static boolean isBackPressed = false;
  private final String LOG_CAT_TAG = "VUTbasedebug";
  @Inject
  Logger logger;
  private ActivityComponent mActivityComponent;
  private long mActivityId;
  private SharedPreferences prefs;
  private IndexService indexService;
  private FirebaseRemoteConfig firebaseRemoteConfig;

  private ObservableLifeCycleManager observableLifeCycleManager;

  public static void setExtraCaption(Intent i, String caption) {
    i.putExtra(EXTRA_CAPTION, caption);
  }

  public ObservableLifeCycleManager getObservableLifeCycleManager() {
    return observableLifeCycleManager;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    logToLogCat("Starting activity -" + this.getClass().getName());
    logAllIntentExtras("ActivityIntent", getIntent());

    initActivityModule(savedInstanceState);

    indexService = ((VutIndexApplication) getApplicationContext()).getComponent().indexService();
    firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
  }

  @Override
  protected void onStart() {
    super.onStart();
    applicationWillEnterForeground();

  }

  private void applicationWillEnterForeground() {
    firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

    if (isAppWentToBg) {
      isAppWentToBg = false;
      Log.d("VUTdebug", "Aplikace je v popředí");
    }
  }

  @Override
  protected void onStop() {
    super.onStop();

    applicationWillEnterBackground();
  }

  private void applicationWillEnterBackground() {
    firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    if (!isWindowFocused) {
      isAppWentToBg = true;
      Log.d("VUTdebug", "Aplikace je v pozadí");
    }
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    isWindowFocused = hasFocus;
    if (isBackPressed && !hasFocus) {
      isBackPressed = false;
      isWindowFocused = true;
    }

    super.onWindowFocusChanged(hasFocus);
  }

  private void initActivityModule(Bundle savedInstanceState) {
    mActivityId = savedInstanceState != null ?
            savedInstanceState.getLong(KEY_ACTIVITY_ID) : NEXT_ID.getAndIncrement();
    ConfigPersistentComponent configPersistentComponent;

    ConfigPersistentComponent config = sComponentsArray.get(mActivityId);

    if (config == null) {
      logToLogCat("Creating new ConfigPersistentComponent");
      configPersistentComponent = DaggerConfigPersistentComponent.builder()
              .applicationComponent(VutIndexApplication.get(this).getComponent())
              .build();
      sComponentsArray.put(mActivityId, configPersistentComponent);
    } else {
      logToLogCat("Reusing ConfigPersistentComponent ");
      configPersistentComponent = config;
    }

    this.observableLifeCycleManager = lcManagers.get(mActivityId);

    if (this.observableLifeCycleManager == null) {
      this.observableLifeCycleManager = new ObservableLifeCycleManager();
    }


    mActivityComponent = configPersistentComponent.activityComponent(new ActivityModule(this));
    mActivityComponent.inject(this);
  }

  @Override
  protected void onResume() {
    this.observableLifeCycleManager.raiseOnResume();
    super.onResume();
  }

  @Override
  protected void onPause() {
    this.observableLifeCycleManager.raiseOnPause();
    super.onPause();
  }

  @Override
  public void onBackPressed() {
    isBackPressed = true;
    super.onBackPressed();
  }

  @Override
  public boolean onSupportNavigateUp() {
    onBackPressed();
    return true;
  }

  @Override
  public void setContentView(View view) {
    super.setContentView(view);
    ButterKnife.bind(this);
    inject();
    logToLogCat("ButterKnife bounded");

  }

  @Override
  public void setContentView(int layoutResID) {
    super.setContentView(layoutResID);
    ButterKnife.bind(this);
    inject();
    logToLogCat("ButterKnife bounded");

  }

  protected ActivityComponent activityComponent() {
    return mActivityComponent;
  }


  @Override
  protected void onDestroy() {
    prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

    if (!isChangingConfigurations()) {
      logToLogCat("Clearing ConfigPersistentComponent");
      sComponentsArray.remove(mActivityId);
      lcManagers.remove(mActivityId);
    }
    super.onDestroy();
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putLong(KEY_ACTIVITY_ID, mActivityId);

  }


  private void logAllIntentExtras(String extrasPrefix, Intent intentToLog) {
    logToLogCat("logAllIntentExtras");

    if (intentToLog != null) {
      try {
        Bundle bundle = intentToLog.getExtras();

        if (bundle != null) {
          for (String key : bundle.keySet()) {
            Object value = bundle.get(key);

            if (value == null) {
              logToLogCat(String.format("logAllIntentExtras - %s -- %s -- (%s)", key, "NULL", "NULL"));
            } else {
              logToLogCat(String.format("logAllIntentExtras - %s -- %s -- (%s)", key, value.toString(), value.getClass().getName()));
            }
          }
        } else {
          logToLogCat("logAllIntentExtras " + extrasPrefix + " empty intent bundle");
        }
      } catch (Exception e) {
        logger.logError("VUTdebug", "logAllIntentExtras", e);
      }
    } else {
      logToLogCat("logAllIntentExtras " + extrasPrefix + " empty intent extras");
    }
  }

  protected void logToLogCat(String message) {
    try {
      Log.i(LOG_CAT_TAG, this.getClass().getName() + ", Message: " + message);
    } catch (Exception e) {
      logger.logError("VUTdebug", "logToLogCat", e);
    }
  }

  protected void allowBackAppIcon() {
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

  }

  protected abstract void inject();

}