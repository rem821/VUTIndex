package prvnimilion.vutindex.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import butterknife.ButterKnife;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.ActivityAboutBinding;
import prvnimilion.vutindex.mvp.presenters.AboutPresenter;
import prvnimilion.vutindex.mvp.views.AboutActivityMvpView;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;

public class AboutActivity extends VutIndexBaseActivity implements AboutActivityMvpView {

  @Inject
  public AboutPresenter presenter;
  private ActivityAboutBinding binding;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_about, getWindow().getDecorView().findViewById(android.R.id.content), false);
    setContentView(binding.getRoot());
    binding.setPresenter(presenter);
    presenter.attachView(this, savedInstanceState == null);

    ButterKnife.bind(this);
    setSupportActionBar(findViewById(R.id.about_activity_toolbar));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setTitle(this.getString(R.string.about_us));

  }

  public void showToast(String content) {
    Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
  }

  @Override
  public Activity getActivity() {
    return this;
  }

  @Override
  public Context getContext() {
    return getContext();
  }

  @Override
  protected void inject() {
    activityComponent().inject(this);
  }

}
