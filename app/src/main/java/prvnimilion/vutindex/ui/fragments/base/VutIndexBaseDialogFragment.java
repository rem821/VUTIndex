package prvnimilion.vutindex.ui.fragments.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import prvnimilion.vutindex.VutIndexApplication;
import prvnimilion.vutindex.di.components.ConfigPersistentComponent;
import prvnimilion.vutindex.di.components.DaggerConfigPersistentComponent;
import prvnimilion.vutindex.di.components.FragmentComponent;
import prvnimilion.vutindex.di.modul.FragmentModule;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;

public abstract class VutIndexBaseDialogFragment extends DialogFragment implements ObservableLifeCycleManager.IObservableLifeCycleManager {
  private ObservableLifeCycleManager observableLifeCycleManager;
  private FragmentComponent mFragmentComponent;

  public ObservableLifeCycleManager getObservableLifeCycleManager() {
    return observableLifeCycleManager;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initFragmentModule(savedInstanceState);
    this.observableLifeCycleManager = new ObservableLifeCycleManager();
    inject();
  }

  private void initFragmentModule(Bundle savedInstanceState) {
    ConfigPersistentComponent configPersistentComponent = DaggerConfigPersistentComponent.builder()
            .applicationComponent(VutIndexApplication.get(getContext()).getComponent())
            .build();


    mFragmentComponent = configPersistentComponent.fragmentComponent(new FragmentModule(this));
  }

  protected FragmentComponent fragmentComponent() {
    return mFragmentComponent;
  }


  @Override
  public void onResume() {
    this.observableLifeCycleManager.raiseOnResume();
    super.onResume();
  }

  @Override
  public void onPause() {
    this.observableLifeCycleManager.raiseOnPause();
    super.onPause();
  }

  protected abstract void inject();
}
