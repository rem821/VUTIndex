package prvnimilion.vutindex.ui.fragments.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import prvnimilion.vutindex.VutIndexApplication;
import prvnimilion.vutindex.di.components.ConfigPersistentComponent;
import prvnimilion.vutindex.di.components.DaggerConfigPersistentComponent;
import prvnimilion.vutindex.di.components.FragmentComponent;
import prvnimilion.vutindex.di.modul.FragmentModule;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;

public abstract class VutIndexBaseFragment extends Fragment implements ObservableLifeCycleManager.IObservableLifeCycleManager {
  private FragmentComponent mFragmentComponent;
  private ObservableLifeCycleManager observableLifeCycleManager;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    initFragmentModule(savedInstanceState);
    this.observableLifeCycleManager = new ObservableLifeCycleManager();

    inject();
  }

  protected abstract void inject();

  private void initFragmentModule(Bundle savedInstanceState) {
    ConfigPersistentComponent configPersistentComponent = DaggerConfigPersistentComponent.builder()
            .applicationComponent(VutIndexApplication.get(getContext()).getComponent())
            .build();


    mFragmentComponent = configPersistentComponent.fragmentComponent(new FragmentModule(this));
  }

  protected FragmentComponent fragmentComponent() {
    return mFragmentComponent;
  }

  public ObservableLifeCycleManager getObservableLifeCycleManager() {
    return observableLifeCycleManager;
  }

  @Override
  public void onResume() {
    this.observableLifeCycleManager.raiseOnResume();
    super.onResume();
  }

  @Override
  public void onPause() {
    this.observableLifeCycleManager.raiseOnPause();
    super.onPause();
  }

}
