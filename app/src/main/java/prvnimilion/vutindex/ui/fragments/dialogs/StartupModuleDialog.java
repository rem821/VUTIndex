package prvnimilion.vutindex.ui.fragments.dialogs;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.FragmentDialogStartupModuleDialogBinding;
import prvnimilion.vutindex.pojo.SharedPrefsEnums;
import prvnimilion.vutindex.pojo.StartupModules;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.ui.fragments.base.VutIndexBaseDialogFragment;

public class StartupModuleDialog extends VutIndexBaseDialogFragment {
  @Inject
  public RxEventBus eventBus;

  private FragmentDialogStartupModuleDialogBinding binding;
  private SharedPreferences prefs;

  public static StartupModuleDialog newInstance() {
    StartupModuleDialog fragment = new StartupModuleDialog();
    Bundle args = new Bundle();

    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void inject() {
    fragmentComponent().inject(this);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_dialog_startup_module_dialog, null, false);
    binding.setFragment(this);

    prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

    colorCurrentOne(prefs.getString(SharedPrefsEnums.startupModule.toString(), "Dashboard"));
    return binding.getRoot();
  }

  public void dashboardClicked(View v) {
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(SharedPrefsEnums.startupModule.toString(), StartupModules.Dashboard.toString());
    editor.apply();
    getDialog().dismiss();
  }

  public void indexClicked(View v) {
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(SharedPrefsEnums.startupModule.toString(), StartupModules.Index.toString());
    editor.apply();
    getDialog().dismiss();
  }

  public void intraportalClicked(View v) {
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(SharedPrefsEnums.startupModule.toString(), StartupModules.Intra.toString());
    editor.apply();
    getDialog().dismiss();
  }

  public void studisClicked(View v) {
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(SharedPrefsEnums.startupModule.toString(), StartupModules.Studis.toString());
    editor.apply();
    getDialog().dismiss();
  }

  private void colorCurrentOne(String startupModule) {
    if (startupModule.matches(StartupModules.Dashboard.toString())) {
      binding.dashboardActivityCaptionCl.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.DARKEN);
      binding.dashboardActivityCaption.setTextColor(Color.WHITE);
    } else if (startupModule.matches(StartupModules.Index.toString())) {
      binding.indexActivityCaptionCl.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.DARKEN);
      binding.indexActivityCaption.setTextColor(Color.WHITE);
    } else if (startupModule.matches(StartupModules.Intra.toString())) {
      binding.webviewActivityIntraCaptionCl.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.DARKEN);
      binding.webviewActivityIntraCaption.setTextColor(Color.WHITE);
    } else if (startupModule.matches(StartupModules.Studis.toString())) {
      binding.webviewActivityStudisCaptionCl.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.DARKEN);
      binding.webviewActivityStudisCaption.setTextColor(Color.WHITE);
    }
  }
}

