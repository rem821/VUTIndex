package prvnimilion.vutindex.ui.fragments.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.databinding.FragmentDialogDonationsDialogBinding;
import prvnimilion.vutindex.pojo.messages.DonationMessage;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.ui.fragments.base.VutIndexBaseDialogFragment;

public class DonationsDialog extends VutIndexBaseDialogFragment {
  @Inject
  public RxEventBus eventBus;
  private FragmentDialogDonationsDialogBinding binding;

  public static DonationsDialog newInstance() {
    DonationsDialog fragment = new DonationsDialog();
    Bundle args = new Bundle();

    fragment.setArguments(args);
    return fragment;
  }

  @Override
  protected void inject() {
    fragmentComponent().inject(this);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_dialog_donations_dialog, null, false);
    binding.setFragment(this);

    return binding.getRoot();
  }

  public void smallDonation(View v) {
    eventBus.post(new DonationMessage("small_donation"));
    getDialog().dismiss();
  }

  public void mediumDonation(View v) {
    eventBus.post(new DonationMessage("medium_donation"));
    getDialog().dismiss();
  }

  public void bigDonation(View v) {
    eventBus.post(new DonationMessage("big_donation"));
    getDialog().dismiss();
  }

}

