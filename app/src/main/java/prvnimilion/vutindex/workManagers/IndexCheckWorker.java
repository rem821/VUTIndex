package prvnimilion.vutindex.workManagers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import prvnimilion.vutindex.BuildConfig;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.VutIndexApplication;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.pojo.Difference;
import prvnimilion.vutindex.pojo.DifferenceEnums;
import prvnimilion.vutindex.pojo.Index;
import prvnimilion.vutindex.services.IndexService;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.tools.VutLogin;
import prvnimilion.vutindex.ui.activities.IndexActivity;
import prvnimilion.vutindex.ui.activities.WebViewActivity;


public class IndexCheckWorker extends Worker {

  @Inject
  RxEventBus eventBus;
  @Inject
  DbOpenHelper db;
  @Inject
  Logger logger;
  @Inject
  IndexService indexService;
  @Inject
  VutLogin vutLogin;

  Context appCntx;
  List<Difference> differences = new ArrayList<>();

  public IndexCheckWorker(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
    super(appContext, workerParams);
    this.appCntx = appContext;
  }

  @NonNull
  @Override
  public Result doWork() {
    Log.d("VUTdebug", "Worker manager has started to work!");
    try {
      vutLogin = ((VutIndexApplication) getApplicationContext()).getComponent().vutLogin();
      indexService = ((VutIndexApplication) getApplicationContext()).getComponent().indexService();

      Index dbIndex = indexService.getIndexFromDb();
      differences = vutLogin.refreshData(dbIndex);

      if (differences == null) return Worker.Result.success();
      for (int i = 0; i < differences.size(); i++) {
        Log.d("VUTdebug", differences.get(i).toString());
      }
      if (differences.size() != 0) {
        sendNotification(differences);
      }
      return Worker.Result.success();
    } catch (Exception e) {
      Log.e("VUTdebug", "IndexCheckService couldn't complete", e);
      return Worker.Result.failure();
    }
  }

  private void sendNotification(List<Difference> differences) {
    appCntx = getApplicationContext();
    FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(appCntx);

    String NOTIFICATION_CHANNEL_ID = "1";

    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(appCntx, NOTIFICATION_CHANNEL_ID);

    if (differences.get(0).getDifferenceType().matches(DifferenceEnums.passed.toString())) {
      String notifString;
      if (differences.get(0).isPassed()) {
        notifString = "Předmět " + differences.get(0).getSubject() + " uspěšně absolvován!";
      } else {
        notifString = "Předmět " + differences.get(0).getSubject() + " neabsolvován!";
      }

      Intent intent = new Intent(appCntx, IndexActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      PendingIntent pendingIntent = PendingIntent.getActivity(appCntx, 0, intent, 0);

      notificationBuilder
              .setAutoCancel(true)
              .setSmallIcon(R.drawable.notification_icon)
              .setColor(Color.RED)
              .setLargeIcon(BitmapFactory.decodeResource(appCntx.getResources(), R.mipmap.ic_launcher))
              .setWhen(System.currentTimeMillis())
              .setPriority(Notification.PRIORITY_MAX)
              .setContentTitle("Výsledek zkoušky:")
              .setContentIntent(pendingIntent)
              .setContentText(notifString);

      if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "Passed: " + differences.get(0).isPassed());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, differences.get(0).getSubject());
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_UP, bundle);
      }

    } else if (differences.get(0).getDifferenceType().matches(DifferenceEnums.credit.toString())) {
      String notifString;
      if (differences.get(0).isCreditGiven()) {
        notifString = "Zápočet z " + differences.get(0).getSubject() + " udělen!";
      } else {
        notifString = "Zápočet z " + differences.get(0).getSubject() + " neudělen!";
      }

      Intent intent = new Intent(appCntx, IndexActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      PendingIntent pendingIntent = PendingIntent.getActivity(appCntx, 0, intent, 0);

      notificationBuilder
              .setAutoCancel(true)
              .setSmallIcon(R.drawable.notification_icon)
              .setColor(Color.RED)
              .setLargeIcon(BitmapFactory.decodeResource(appCntx.getResources(), R.mipmap.ic_launcher))
              .setWhen(System.currentTimeMillis())
              .setPriority(Notification.PRIORITY_MAX)
              .setContentTitle("Zápočet:")
              .setContentIntent(pendingIntent)
              .setContentText(notifString);

      if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "Credit given:" + differences.get(0).isCreditGiven());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, differences.get(0).getSubject());
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_UP, bundle);
      }

    } else if (differences.get(0).getDifferenceType().matches(DifferenceEnums.points.toString())) {
      Intent intent = new Intent(appCntx, IndexActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      PendingIntent pendingIntent = PendingIntent.getActivity(appCntx, 0, intent, 0);

      notificationBuilder
              .setAutoCancel(true)
              .setSmallIcon(R.drawable.notification_icon)
              .setColor(Color.RED)
              .setLargeIcon(BitmapFactory.decodeResource(appCntx.getResources(), R.mipmap.ic_launcher))
              .setWhen(System.currentTimeMillis())
              .setPriority(Notification.PRIORITY_MAX)
              .setContentTitle("Nové body:")
              .setContentIntent(pendingIntent)
              .setContentText(String.format("%s bodů z %s na ISu!", differences.get(0).getPointsGiven(), differences.get(0).getSubject()));

      if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "New Points!");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, differences.get(0).getSubject());
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_UP, bundle);
      }

    } else if (differences.get(0).getDifferenceType().matches(DifferenceEnums.message.toString())) {
      Intent intent = new Intent(appCntx, WebViewActivity.class);
      intent.putExtra(WebViewActivity.EXTRA_WEBVIEW_URL, "https://www.vutbr.cz/login/intra");
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      PendingIntent pendingIntent = PendingIntent.getActivity(appCntx, 0, intent, 0);

      notificationBuilder
              .setAutoCancel(true)
              .setSmallIcon(R.drawable.notification_icon)
              .setColor(Color.RED)
              .setLargeIcon(BitmapFactory.decodeResource(appCntx.getResources(), R.mipmap.ic_launcher))
              .setWhen(System.currentTimeMillis())
              .setPriority(Notification.PRIORITY_MAX)
              .setContentIntent(pendingIntent)
              .setContentText("Nová VUTzpráva");

      if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "New Message!");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_UP, bundle);
      }
    }
    NotificationManager notificationManager = (NotificationManager) appCntx.getSystemService(Context.NOTIFICATION_SERVICE);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

      int importance = NotificationManager.IMPORTANCE_HIGH;
      NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
      notificationChannel.enableLights(true);
      notificationChannel.setLightColor(Color.RED);
      notificationChannel.enableVibration(true);
      notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
      notificationChannel.setVibrationPattern(new long[]{100, 200});
      assert notificationManager != null;
      notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
      notificationManager.createNotificationChannel(notificationChannel);

    }

    assert notificationBuilder != null;
    notificationManager.notify(0, notificationBuilder.build());

    Vibrator v = (Vibrator) appCntx.getSystemService(Context.VIBRATOR_SERVICE);
    if (v != null) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        v.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE));
      } else {
        v.vibrate(300);
      }
    }
  }
}
