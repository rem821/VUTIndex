package prvnimilion.vutindex;

/**
 * Created by stand on 23.08.2017.
 */

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;
import prvnimilion.vutindex.di.components.ApplicationComponent;
import prvnimilion.vutindex.di.components.DaggerApplicationComponent;
import prvnimilion.vutindex.di.modul.ApplicationModule;


public class VutIndexApplication extends Application {
  private ApplicationComponent mApplicationComponent;

  public static VutIndexApplication get(Context context) {
    return (VutIndexApplication) context.getApplicationContext();
  }

  @Override
  public void onCreate() {
    super.onCreate();
    getComponent().inject(this);
  }

  @Override
  protected void attachBaseContext(Context context) {
    super.attachBaseContext(context);
    MultiDex.install(this);
  }

  public ApplicationComponent getComponent() {
    if (mApplicationComponent == null) {
      mApplicationComponent = DaggerApplicationComponent.builder()
              .applicationModule(new ApplicationModule(this))
              .build();
    }
    return mApplicationComponent;
  }

  @SuppressWarnings("unused")
  // Needed to replace the component with a test specific one
  public void setComponent(ApplicationComponent applicationComponent) {
    mApplicationComponent = applicationComponent;
  }

  @Override
  public void onTerminate() {
    getComponent().dbHelper().closeSingleton();
    super.onTerminate();

  }
}
