package prvnimilion.vutindex.services;

import android.util.Log;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.data.db.dataClasses.IT_INDEXDB;
import prvnimilion.vutindex.data.db.dataClasses.IT_SEMESTERDB;
import prvnimilion.vutindex.data.db.dataClasses.IT_SUBJECTDB;
import prvnimilion.vutindex.pojo.Index;
import prvnimilion.vutindex.pojo.Semester;
import prvnimilion.vutindex.pojo.Subject;


public class IndexService {
  private DbOpenHelper db;

  @Inject
  public IndexService(DbOpenHelper db) {
    this.db = db;
  }

  public IT_INDEXDB getIndex() throws SQLException {
    return db.getIT_INDEXDBDao().queryForAll().get(0);
  }

  public List<IT_SEMESTERDB> getSemesters(UUID indexId) throws SQLException {
    return db.getIT_SEMESTERDBDao().queryForEq(IT_SEMESTERDB.TABLE_ID_COLUMN, indexId);
  }

  public List<IT_SUBJECTDB> getSubjects(UUID semesterId) throws SQLException {
    return db.getIT_SUBJECTDBDao().queryForEq(IT_SUBJECTDB.TABLE_ID_COLUMN, semesterId);
  }

  public void deleteIndexFromDb() throws SQLException {
    if (db.getIT_INDEXDBDao().queryForAll().size() > 0)
      db.getIT_INDEXDBDao().delete(db.getIT_INDEXDBDao().queryForAll());

    if (db.getIT_SEMESTERDBDao().queryForAll().size() > 0)
      db.getIT_SEMESTERDBDao().delete(db.getIT_SEMESTERDBDao().queryForAll());

    if (db.getIT_SUBJECTDBDao().queryForAll().size() > 0)
      db.getIT_SUBJECTDBDao().delete(db.getIT_SUBJECTDBDao().queryForAll());
  }

  public void saveIndexToDb(Index index) throws SQLException {
    Log.d("VUTdebug", "Saving index to db...");
    if (index.getSemesters().size() == 0) return;
    deleteIndexFromDb();

    db.getIT_INDEXDBDao().create(new IT_INDEXDB(index.getSemesters().size()));

    UUID semesterTableId = db.getIT_INDEXDBDao().queryForAll().get(0).getINDEX_ID();

    for (Semester semester : index.getSemesters()) {
      IT_SEMESTERDB itSemesterdb = new IT_SEMESTERDB(semesterTableId, semester.getSubjects().size());
      db.getIT_SEMESTERDBDao().create(itSemesterdb);

      UUID subjectTableId = itSemesterdb.getSEMESTER_ID();
      for (Subject subject : semester.getSubjects()) {
        IT_SUBJECTDB subjectdb = new IT_SUBJECTDB(subjectTableId,
                subject.getSubjectFullName(),
                subject.getSubjectShortName(),
                subject.getType(),
                subject.getCredits(),
                subject.getCompletion(),
                subject.isCreditGiven(),
                subject.getPoints(),
                subject.getGrade(),
                subject.getTermTime(),
                subject.isPassed(),
                subject.getVsp());

        db.getIT_SUBJECTDBDao().create(subjectdb);
      }
    }
  }

  public Index getIndexFromDb() throws SQLException {
    Log.d("VUTdebug", "getIndexFromDb... ");

    Index index = new Index();

    if (db.getIT_INDEXDBDao().queryForAll().size() == 0) {
      return null;
    }
    List<IT_SEMESTERDB> semesterDbList = db.getIT_SEMESTERDBDao().queryForAll();
    if (semesterDbList.size() == 0) {
      return null;
    }

    for (int i = 0; i < semesterDbList.size(); i++) {
      Semester semester = new Semester();

      IT_SEMESTERDB semesterDb = semesterDbList.get(i);
      UUID semesterID = semesterDb.getSEMESTER_ID();

      List<IT_SUBJECTDB> subjectDbList = db.getIT_SUBJECTDBDao().queryForEq(IT_SUBJECTDB.SUBJECT_ID_COLUMN, semesterID);
      for (int j = 0; j < subjectDbList.size(); j++) {
        IT_SUBJECTDB subjectdb = subjectDbList.get(j);
        Subject subject = new Subject();
        subject.setSubjectFullName(subjectdb.getSUBJECT_FULL_NAME());
        subject.setSubjectShortName(subjectdb.getSUBJECT_SHORT_NAME());
        subject.setType(subjectdb.getTYPE());
        subject.setCredits(subjectdb.getCREDITS());
        subject.setCompletion(subjectdb.getCOMPLETION());
        subject.setCreditGiven(subjectdb.getCREDIT_GIVEN());
        subject.setPoints(subjectdb.getPOINTS());
        subject.setGrade(subjectdb.getGRADE());
        subject.setTermTime(subjectdb.getTERM_TIME());
        subject.setPassed(subjectdb.getPASSED());
        subject.setVsp(subjectdb.getVSP());
        semester.addSubject(subject);
      }
      index.addSemester(semester);
    }

    return index;
  }

}