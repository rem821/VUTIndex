package prvnimilion.vutindex.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.impl.cookie.BasicClientCookie;
import io.reactivex.annotations.Nullable;
import prvnimilion.vutindex.BuildConfig;
import prvnimilion.vutindex.VutIndexApplication;
import prvnimilion.vutindex.pojo.Difference;
import prvnimilion.vutindex.pojo.DifferenceEnums;
import prvnimilion.vutindex.pojo.Index;
import prvnimilion.vutindex.pojo.Semester;
import prvnimilion.vutindex.pojo.SharedPrefsEnums;
import prvnimilion.vutindex.pojo.Subject;
import prvnimilion.vutindex.pojo.Urls;
import prvnimilion.vutindex.pojo.messages.IndexChangeMessage;
import prvnimilion.vutindex.services.IndexService;

public class VutLogin {


    private final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";
    private Map<String, String> headers = new HashMap<>();
    private Map<String, String> cookies;
    private Document indexDoc;
    private Document messagesDoc;
    private boolean logedIn = true;
    private List<Difference> differences = new ArrayList<>();
    private SharedPreferences prefs;
    private RxEventBus eventBus;
    private IndexService indexService;
    private Context appCntx;
    private PersistentCookieStore myCookieStore;
    private boolean online = false;

    public VutLogin(Context appCntx, RxEventBus eventBus) {
        this.appCntx = appCntx;
        this.eventBus = eventBus;

        AsyncHttpClient myClient = new AsyncHttpClient();
        myCookieStore = new PersistentCookieStore(appCntx);
        myClient.setCookieStore(myCookieStore);

        cookies = new HashMap<>();
    }

    public boolean LogIn(String username, String passwd, RxEventBus eventBus, IndexService indexService) {
        Log.d("VUTdebug", "Log in...");
        this.eventBus = eventBus;
        this.indexService = indexService;

        try {
            logedIn = true;
            ultimateLogIn(username, passwd);
        } catch (Exception e) {
            logedIn = false;
            Log.e("VUTdebug", "Login error: " + e.toString());
            if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
                Crashlytics.log(Log.ERROR, "Login error: ", e.toString());
            }
        }

        if (logedIn) {
            try {
                Log.d("VUTdebug", "Loged in successfully");
                if (indexService == null) {
                    indexService = ((VutIndexApplication) appCntx.getApplicationContext()).getComponent().indexService();
                }
                Index dbIndex = indexService.getIndexFromDb();
                compareIndexes(ScrapeIndex(), dbIndex);
                checkNewMessages(getPageDoc(Urls.messagesUrl));
            } catch (Exception e) {
                Log.e("VUTdebug", "Compare indexes error: " + e.toString());
                if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
                    Crashlytics.log(Log.ERROR, "Compare indexes error: ", e.toString());
                    Crashlytics.log(Log.ERROR, "crashed index: ", indexDoc.baseUri());
                }
            }
            return true;
        }
        return false;
    }

    private Map<String, String> ultimateLogIn(String username, String password) throws Exception {
        Map<String, String> values = new HashMap<>();
        int contentLength;

        Log.d("VUTdebug", "Stahuji aktuálni data z indexu.... ");
        Log.d("SCRAPEdebug", "Připojuji se na přihlašovací stránku.... ");
        Connection.Response res = Jsoup.connect(Urls.loginUrl)
                .followRedirects(true)
                .method(Connection.Method.GET)
                .userAgent(USER_AGENT)
                .execute();
        cookies = res.cookies();
        cookies.put("fontsLoaded", "true");

        Log.d("SCRAPEdebug", "Ukládám cookies z přihlašovaci stránky.... ");
        System.out.println(res.cookies());

//      Vyplnění přihlašovacích údajů
        Document loginPage = res.parse();
        Element loginform = loginPage.getElementById("login_form");
        Elements inputElements = loginform.getElementsByTag("input");
        for (Element inputElement : inputElements) {
            String key = inputElement.attr("name");
            String value = inputElement.attr("value");

            if (key.equals("LDAPlogin"))
                value = username;
            else if (key.equals("LDAPpasswd"))
                value = password;
            else if (key.equals("pamatovat_login"))
                continue;

            String klic = URLEncoder.encode(key, "UTF-8");
            String hodnota = URLEncoder.encode(value, "UTF-8");
            values.put(klic, hodnota);
        }
        values.put("login", "");

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, String> param : values.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(param.getKey());
            postData.append('=');
            postData.append(String.valueOf(param.getValue()));
        }
        contentLength = postData.toString().getBytes("UTF-8").length;

        String cookieString = "";
        for (Map.Entry<String, String> cookie : cookies.entrySet()) {
            cookieString = cookieString.concat(String.format("%s=%s; ", cookie.getKey(), cookie.getValue()));
        }
        cookieString = cookieString.substring(0, cookieString.length() - 2);

        Log.d("SCRAPEdebug", "Odesílám požadavek na přihlášeni.... ");
        URL url = new URL(Urls.requestUrl);

        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) con;
        http.setRequestMethod("POST");

        http.setFixedLengthStreamingMode(contentLength);
        http.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        http.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
        http.setRequestProperty("Accept-Language", "cs,sk;q=0.8,en-US;q=0.5,en;q=0.3");
        http.setRequestProperty("Connection", "keep-alive");
        http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        http.setRequestProperty("Host", "www.vutbr.cz");
        http.setRequestProperty("Referer", "https://www.vutbr.cz/login/intra?aid_redir=1");
        http.setRequestProperty("Upgrade-Insecure-Requests", "1");
        http.setRequestProperty("Content-Length", String.valueOf(contentLength));
        http.setRequestProperty("User-Agent", USER_AGENT);
        http.setRequestProperty("Cookie", cookieString);

        http.setDoInput(true);
        http.setDoOutput(true);
        http.setInstanceFollowRedirects(false);
        http.connect();

        OutputStreamWriter writer = new OutputStreamWriter(http.getOutputStream(), "UTF-8");
        writer.write(postData.toString());
        writer.close();

        int responseCode = http.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        Log.d("SCRAPEdebug", "Přidávám cookies z přihlášení.... ");
        List<String> httpCookies = http.getHeaderFields().get("Set-Cookie");
        for (String cookie : httpCookies) {
            String name = cookie.split("=")[0];
            String value = cookie.split("=")[1];
            value = value.split(";")[0];
            cookies.put(name, value);
        }
        if (cookies.get("portal_is_logged_in") == null) {
            logedIn = false;
            return cookies;
        } else if (!cookies.get("portal_is_logged_in").matches("1")) {
            logedIn = false;
            return cookies;
        }

        Log.d("SCRAPEdebug", "Otevírám index.... ");
        Connection.Response elIndex = Jsoup.connect(Urls.index)
                .followRedirects(true)
                .method(Connection.Method.GET)
                .headers(headers)
                .headers(headers)
                .timeout(10 * 1000)
                .cookies(cookies)
                .execute();

        cookies.putAll(elIndex.cookies());

        saveCookies();

        indexDoc = elIndex.parse();

        return cookies;
    }

    private Index ScrapeIndex() {
        Log.d("SCRAPEdebug", "Scrapuji informační systém.... ");
        Index newIndex = new Index();
        differences.clear();

        //Pro testování custom indexů
//        AssetManager assetManager = appCntx.getAssets();
//        InputStream input;
//        String text = "";
//        try {
//            input = assetManager.open("index2.html");
//            int size = input.available();
//            byte[] buffer = new byte[size];
//            input.read(buffer);
//            input.close();
//            text = new String(buffer);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        indexDoc = Jsoup.parse(text);

        Element headerContainer = indexDoc.getElementsByClass("main-content").first();
        Elements headers = headerContainer.getElementsByTag("h3");
        Elements tableContainers = indexDoc.getElementsByClass("table table-bordered table-middle");
        Elements indexTables = new Elements();
        for (Element tableContainer : tableContainers) {
            indexTables.add(tableContainer.getElementsByTag("tbody").first());
        }
        List<Elements> elementsList = new ArrayList<>();
        Elements indexTableElements = new Elements();
        for (Element indexTable : indexTables) {
            indexTableElements.addAll(indexTable.getAllElements());
            elementsList.add(indexTableElements);
            indexTableElements = new Elements();
        }

        Semester semester;
        for (int i = 0; i < indexTables.size(); i++) {
            semester = new Semester();

            //Dělá hlouposti, občas tam ta hlavička chybí a pak to padá
            //Element header = headers.get(i);
            //semester.setSemesterHeader(header.text());

            for (Element element : elementsList.get(i)) {
                if (element.className().contains("pov")) {
                    Elements complete = element.getElementsByClass("center");
                    List<String> parsed = complete.eachText();

                    String fullName = element.select("[title=Detail zapsaného předmětu]").text();
                    String shortName = parsed.get(0);
                    String type = parsed.get(2);
                    String credits = parsed.get(3);

                    String vsp = "false";
                    if (parsed.get(4).contains("Ano") || parsed.get(4).contains("ano"))
                        vsp = "true";

                    String completion = parsed.get(5);

                    String creditGiven = "";
                    if (parsed.get(6).contains("ano"))
                        creditGiven = "true";
                    else if (parsed.get(6).contains("ne"))
                        creditGiven = "false";

                    String points = parsed.get(7);

                    String grade = "";
                    if (!parsed.get(8).matches(""))
                        grade = parsed.get(8).substring(0, 1);

                    String termTime = "";
                    if (!parsed.get(9).matches(""))
                        termTime = parsed.get(9);

                    String passed = "false";
                    if (parsed.get(10).contains("ano"))
                        passed = "true";

                    Log.d("SCRAPEdebug", String.format("zkratka:%s název:%s typ:%s kredity:%s ukončení:%s započteno:%s body:%s známka:%s termín:%s absolvováno:%s vsp:%s", shortName, fullName, type, credits, completion, creditGiven, points, grade, termTime, passed, vsp));

                    Subject subject = new Subject(fullName, shortName, type, credits, completion, creditGiven, points, grade, termTime, passed, vsp);
                    semester.addSubject(subject);
                }
            }
            newIndex.addSemester(semester);
        }
        Log.d("VUTdebug", "Scraping done succesfully");
        return newIndex;
    }

    public List<Difference> refreshData(@Nullable Index dbIndex) throws Exception {
        Log.d("VUTdebug", "Refresh data...");

        if (!isOnline()) {
            Log.d("VUTdebug", "No internet - skipping");
            return new ArrayList<Difference>();
        }

        CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    loadCookies();
                    reloadIndex();
                    latch.countDown();
                } catch (Exception e) {
                    logedIn = false;
                    Log.e("VUTdebug", "Nepodařilo se synchronizovat data s IS: " + e.toString());
                }

            }
        };
        t.start();
        latch.await();

        if (logedIn) {
            Log.d("VUTdebug", "Rychlé přihlášení proběhlo bez problémů.... ");
            saveCookies();
            Index newIndex = ScrapeIndex();
            compareIndexes(newIndex, dbIndex);
            checkNewMessages(getPageDoc(Urls.messagesUrl));

            return differences;
        } else {
            CountDownLatch latch2 = new CountDownLatch(1);
            Thread t2 = new Thread() {
                @Override
                public void run() {
                    try {
                        Log.d("VUTdebug", "Probíhá opětovné přihlášení.... ");
                        prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
                        LogIn(prefs.getString(String.valueOf(SharedPrefsEnums.username), ""), prefs.getString(String.valueOf(SharedPrefsEnums.password), ""), eventBus, indexService);

                        latch2.countDown();
                    } catch (Exception e) {
                        logedIn = false;
                        Log.e("VUTdebug", "Nepodařilo se synchronizovat data s IS po opětovném přihlášení: " + e.toString());
                    }

                }
            };
            t2.start();
            latch2.await();

            if (logedIn) {
                Log.d("VUTdebug", "Opětovné přihlášení bylo úspěšné...");
                saveCookies();

                return differences;
            }
        }

        return null;
    }

    private void reloadIndex() throws Exception {
        Log.d("VUTdebug", "Reload index... ");

        Connection.Response elIndex = Jsoup.connect(Urls.index)
                .followRedirects(true)
                .method(Connection.Method.GET)
                .headers(headers)
                .timeout(10 * 1000)
                .cookies(cookies)
                .execute();

        cookies.putAll(elIndex.cookies());

        if (cookies.get("portal_is_logged_in") == null) {
            logedIn = false;
        } else if (!cookies.get("portal_is_logged_in").matches("1")) {
            logedIn = false;
        }

        indexDoc = elIndex.parse();
    }

    private void compareIndexes(Index newIndex, Index dbIndex) {
        Log.d("VUTdebug", "CompareIndexes=>");
        //Debug moznost
        prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
        if (prefs.getBoolean(SharedPrefsEnums.falseAlarm.toString(), false)) {
            Random rn = new Random();
            newIndex.getSemesters().get(0).getSubjects().get(0).setPoints(String.format("%s", rn.nextInt(50) + 50));
        }

        if (dbIndex == null) {
            Log.d("VUTdebug", "Saved fresh index to db...");
            saveIndexToDb(newIndex);
            return;
        }

        Log.d("VUTdebug", "...Porovnávám s indexem z databáze...");
        if (dbIndex.equals(newIndex)) {
            Log.d("VUTdebug", "...Index beze změny");
            saveIndexToDb(newIndex);
            eventBus.post(new IndexChangeMessage());
        } else {
            Log.d("VUTdebug", "...Změna v indexu");
            saveIndexToDb(newIndex);
            differences = dbIndex.getDifferences();
            eventBus.post(new IndexChangeMessage());
        }
    }

    private Document getPageDoc(String page) throws IOException {
        Log.d("VUTdebug", "loading page: " + page);

        Connection.Response messages = Jsoup.connect(page)
                .followRedirects(true)
                .method(Connection.Method.GET)
                .headers(headers)
                .timeout(10 * 1000)
                .cookies(cookies)
                .execute();

        cookies.putAll(messages.cookies());

        if (cookies.get("portal_is_logged_in") == null) {
            logedIn = false;
        } else if (!cookies.get("portal_is_logged_in").matches("1")) {
            logedIn = false;
        }

        return messagesDoc = messages.parse();
    }

    private void checkNewMessages(Document pageDoc) {
        try {
            Element firstMsg = pageDoc.getElementsByClass("table table-striped table-bordered").first();
            String msgTitle = firstMsg.child(1).child(0).child(0).child(0).text();

            prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
            String lastMsgTitle = prefs.getString(SharedPrefsEnums.lastMsgTitle.toString(), "");
            if (!lastMsgTitle.equals(msgTitle)) {
                differences.add(new Difference(DifferenceEnums.message.toString()));
                Log.d("VUTdebug", "current unread message: " + msgTitle);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(SharedPrefsEnums.lastMsgTitle.toString(), msgTitle);
                editor.apply();
            } else {
                Log.d("VUTdebug", "no new messages");
            }
        } catch (Exception e) {
        }
    }

    private void loadCookies() {
        List<Cookie> cooks = myCookieStore.getCookies();
        for (Cookie cook : cooks) {
            cookies.put(cook.getName(), cook.getValue());
        }
    }

    private void saveCookies() {
        myCookieStore.clear();
        for (Map.Entry<String, String> entry : cookies.entrySet()) {
            BasicClientCookie newCookie = new BasicClientCookie(entry.getKey(), entry.getValue());
            myCookieStore.addCookie(newCookie);
        }
    }

    //Testing purposes only!!
    private void deleteCookies() {
        cookies.clear();
        myCookieStore.clear();
    }

    private void saveIndexToDb(Index index) {
        try {
            if (indexService == null)
                indexService = ((VutIndexApplication) appCntx.getApplicationContext()).getComponent().indexService();
            indexService.saveIndexToDb(index);
        } catch (SQLException e) {
            Log.e("VUTdebug", "saving index to db error: " + e.toString());
        }
    }

    private boolean isOnline() {
        try {
            CountDownLatch latch = new CountDownLatch(1);
            Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        int timeoutMs = 1500;
                        Socket sock = new Socket();
                        SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

                        sock.connect(sockaddr, timeoutMs);
                        sock.close();
                        online = true;
                        latch.countDown();
                    } catch (Exception e) {
                        online = false;
                        Log.e("VUTdebug", "Couldnt check net connection " + e.toString());
                    }
                }
            };
            t.start();
            latch.await();
        } catch (Exception e) {
            online = false;
            Log.e("VUTdebug", "Couldnt check net connection " + e.toString());
        }
        return online;
    }

}
