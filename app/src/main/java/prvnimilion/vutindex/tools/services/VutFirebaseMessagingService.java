package prvnimilion.vutindex.tools.services;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class VutFirebaseMessagingService extends FirebaseMessagingService {
  private LocalBroadcastManager broadcaster;

  @Override
  public void onCreate() {
    broadcaster = LocalBroadcastManager.getInstance(this);
  }

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {

  }
}
