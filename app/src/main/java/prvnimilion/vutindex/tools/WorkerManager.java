package prvnimilion.vutindex.tools;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import prvnimilion.vutindex.workManagers.IndexCheckWorker;

public class WorkerManager {

  public static void startService(Context appCntx, long time) {
    Log.d("VUTdebug", "WorkerManager has been scheduled for: " + time / 60000 + " minutes");
    PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(IndexCheckWorker.class, time, TimeUnit.MILLISECONDS).build();
    WorkManager.getInstance().enqueue(periodicWorkRequest);
  }

  public static void stopService(Context appCntx) {
    Log.d("VUTdebug", "WorkerManager been laid off...");
    WorkManager.getInstance().cancelAllWork();
  }
}
