package prvnimilion.vutindex.tools;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import javax.inject.Inject;

import prvnimilion.vutindex.di.ApplicationContext;


/**
 * Created by stand on 13.08.2017.
 */

public class Logger {
  public final static String TAG = "BTA";
  Context cntx;

  @Inject
  public Logger(@ApplicationContext Context cntx) {
    this.cntx = cntx;
  }

  public void logError(String source, String message, Throwable exception, final String toastText) {
    if (exception != null) {
      exception.printStackTrace();

      if (toastText != null) {
        Runnable myRunnable = () -> Toast.makeText(cntx, toastText, Toast.LENGTH_LONG).show();
        new Handler(Looper.getMainLooper()).post(myRunnable);
      }

      String msg = source + " : " + message;
      android.util.Log.e(TAG, msg, exception);

    }
  }

  public void logError(String source, String message, Throwable exception, final int toastStringId) {
    if (toastStringId != -1) {
      logError(source, message, exception, cntx.getResources().getString(toastStringId));
    } else {
      logError(source, message, exception, null);
    }
  }

  public void logError(String source, String message, Throwable exception) {
    logError(source, message, exception, -1);
  }
}

