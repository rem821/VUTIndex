package prvnimilion.vutindex.pojo;

public class Urls {

  public static String index = "https://www.vutbr.cz/studis/student.phtml?sn=el_index";
  public static String loginUrl = "https://www.vutbr.cz/login/intra";
  public static String requestUrl = "https://www.vutbr.cz/login/in";
  public static String messagesUrl = "https://www.vutbr.cz/intra";

}