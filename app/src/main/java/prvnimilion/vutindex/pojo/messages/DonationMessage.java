package prvnimilion.vutindex.pojo.messages;

public class DonationMessage {

  private String skuId;

  public DonationMessage(String skuId) {
    this.skuId = skuId;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }
}
