package prvnimilion.vutindex.pojo;

import java.util.ArrayList;
import java.util.List;

public class Semester {

  private List<Subject> subjects = new ArrayList<>();
  private String semesterHeader;

  public Semester() {
  }

  public List<Subject> getSubjects() {
    return subjects;
  }

  public void setSubjects(List<Subject> subjects) {
    this.subjects = subjects;
  }

  public void addSubject(Subject subject) {
    this.subjects.add(subject);
  }

  public String getSemesterHeader() {
    return semesterHeader;
  }

  public void setSemesterHeader(String semesterHeader) {
    this.semesterHeader = semesterHeader;
  }
}
