package prvnimilion.vutindex.pojo;

public class Subject {

  private String subjectFullName;
  private String subjectShortName;
  private String type;
  private String credits;
  private String completion;
  private String creditGiven;
  private String points;
  private String grade;
  private String termTime;
  private String passed;
  private String vsp;

  public Subject() {
  }

  public Subject(String subjectFullName, String subjectShortName, String type, String credits, String completion, String creditGiven, String points, String grade, String termTime, String passed, String vsp) {
    this.subjectFullName = subjectFullName;
    this.subjectShortName = subjectShortName;
    this.type = type;
    this.credits = credits;
    this.completion = completion;
    this.creditGiven = creditGiven;
    this.points = points;
    this.grade = grade;
    this.termTime = termTime;
    this.passed = passed;
    this.vsp = vsp;
  }

  public String getSubjectFullName() {
    return subjectFullName;
  }

  public void setSubjectFullName(String subjectFullName) {
    this.subjectFullName = subjectFullName;
  }

  public String getSubjectShortName() {
    return subjectShortName;
  }

  public void setSubjectShortName(String subjectShortName) {
    this.subjectShortName = subjectShortName;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCredits() {
    return credits;
  }

  public void setCredits(String credits) {
    this.credits = credits;
  }

  public String getCompletion() {
    return completion;
  }

  public void setCompletion(String completion) {
    this.completion = completion;
  }

  public String isCreditGiven() {
    return creditGiven;
  }

  public void setCreditGiven(String creditGiven) {
    this.creditGiven = creditGiven;
  }

  public String getPoints() {
    return points;
  }

  public void setPoints(String points) {
    this.points = points;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public String getTermTime() {
    return termTime;
  }

  public void setTermTime(String termTime) {
    this.termTime = termTime;
  }

  public String isPassed() {
    return passed;
  }

  public void setPassed(String passed) {
    this.passed = passed;
  }

  public String getVsp() {
    return vsp;
  }

  public void setVsp(String vsp) {
    this.vsp = vsp;
  }
}
