package prvnimilion.vutindex.pojo;

import java.util.ArrayList;
import java.util.List;

public class Index {

  private List<Semester> semesters;
  private List<Difference> differences;

  public Index() {
    semesters = new ArrayList<>();
    differences = new ArrayList<>();
  }

  public void addSemester(Semester semester) {
    this.semesters.add(semester);
  }

  public List<Semester> getSemesters() {
    return semesters;
  }

  public void setSemesters(List<Semester> semesters) {
    this.semesters = semesters;
  }

  public void resetIndex() {
    semesters = new ArrayList<>();
  }

  public boolean equals(Index newIndex) {
    differences.clear();

    boolean equal = true;
    if (this.getSemesters().size() == newIndex.getSemesters().size()) {
      for (int i = 0; i < this.getSemesters().size(); i++) {
        if (this.getSemesters().get(i).getSubjects().size() == newIndex.getSemesters().get(i).getSubjects().size()) {
          for (int j = 0; j < this.getSemesters().get(i).getSubjects().size(); j++) {
            //zkouska
            if (!this.getSemesters().get(i).getSubjects().get(j).isPassed().matches(newIndex.getSemesters().get(i).getSubjects().get(j).isPassed())) {
              boolean passed = Boolean.valueOf(newIndex.getSemesters().get(i).getSubjects().get(j).isPassed());

              Difference dif = new Difference();
              dif.setDifferenceType(DifferenceEnums.passed.toString());
              dif.setSubject(newIndex.getSemesters().get(i).getSubjects().get(j).getSubjectShortName());
              dif.setPassed(passed);
              differences.add(dif);
              equal = false;
            }
            //zapocet
            if (!this.getSemesters().get(i).getSubjects().get(j).isCreditGiven().matches(newIndex.getSemesters().get(i).getSubjects().get(j).isCreditGiven())) {
              boolean given = Boolean.valueOf(newIndex.getSemesters().get(i).getSubjects().get(j).isCreditGiven());

              Difference dif = new Difference();
              dif.setDifferenceType(DifferenceEnums.credit.toString());
              dif.setSubject(newIndex.getSemesters().get(i).getSubjects().get(j).getSubjectShortName());
              dif.setCreditGiven(given);
              differences.add(dif);
              equal = false;
            }
            //body
            if (!this.getSemesters().get(i).getSubjects().get(j).getPoints().matches(newIndex.getSemesters().get(i).getSubjects().get(j).getPoints())) {
              int oldPoints, newPoints;
              try {
                oldPoints = Integer.valueOf(this.getSemesters().get(i).getSubjects().get(j).getPoints());
                newPoints = Integer.valueOf(newIndex.getSemesters().get(i).getSubjects().get(j).getPoints());
              } catch (Exception e) {
                return false;
              }
              int pointDifference = newPoints - oldPoints;

              Difference dif = new Difference();
              dif.setDifferenceType(DifferenceEnums.points.toString());
              dif.setSubject(newIndex.getSemesters().get(i).getSubjects().get(j).getSubjectShortName());
              dif.setPointsGiven(pointDifference);
              differences.add(dif);
              equal = false;
            }
          }
        } else {
          return false;
        }
      }
    } else {
      return false;
    }
    return equal;
  }

  public List<Difference> getDifferences() {
    return differences;
  }

}
