package prvnimilion.vutindex.pojo;

public class Difference {

  private String differenceType;
  private String subject;
  private boolean creditGiven;
  private int pointsGiven;
  private boolean passed;

  public Difference() {
  }

  public Difference(String differenceType, String subject, boolean creditGiven, int pointsGiven, boolean passed) {
    this.differenceType = differenceType;
    this.subject = subject;
    this.creditGiven = creditGiven;
    this.pointsGiven = pointsGiven;
    this.passed = passed;
  }

  public Difference(String differenceType, String subject, boolean creditGiven) {
    this.differenceType = differenceType;
    this.subject = subject;
    this.creditGiven = creditGiven;
  }

  public Difference(String differenceType, String subject, int pointsGiven) {
    this.differenceType = differenceType;
    this.subject = subject;
    this.pointsGiven = pointsGiven;
  }

  public Difference(String differenceType) {
    this.differenceType = differenceType;
  }

  public String getDifferenceType() {
    return differenceType;
  }

  public void setDifferenceType(String differenceType) {
    this.differenceType = differenceType;
  }

  public boolean isCreditGiven() {
    return creditGiven;
  }

  public void setCreditGiven(boolean creditGiven) {
    this.creditGiven = creditGiven;
  }

  public int getPointsGiven() {
    return pointsGiven;
  }

  public void setPointsGiven(int pointsGiven) {
    this.pointsGiven = pointsGiven;
  }

  public boolean isPassed() {
    return passed;
  }

  public void setPassed(boolean passed) {
    this.passed = passed;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }
}
