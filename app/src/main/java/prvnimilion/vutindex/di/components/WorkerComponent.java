package prvnimilion.vutindex.di.components;


import dagger.Component;
import prvnimilion.vutindex.di.PerWorker;
import prvnimilion.vutindex.di.modul.WorkerModule;
import prvnimilion.vutindex.workManagers.IndexCheckWorker;

@PerWorker
@Component(modules = WorkerModule.class, dependencies = ApplicationComponent.class)
public interface WorkerComponent {
  void inject(IndexCheckWorker worker);
}