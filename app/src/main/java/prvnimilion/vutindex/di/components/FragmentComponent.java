package prvnimilion.vutindex.di.components;

import dagger.Subcomponent;
import prvnimilion.vutindex.di.PerFragment;
import prvnimilion.vutindex.di.modul.FragmentModule;
import prvnimilion.vutindex.ui.fragments.base.VutIndexBaseFragment;
import prvnimilion.vutindex.ui.fragments.dialogs.DonationsDialog;
import prvnimilion.vutindex.ui.fragments.dialogs.StartupModuleDialog;

@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {

  void inject(VutIndexBaseFragment fragment);

  void inject(DonationsDialog fragment);

  void inject(StartupModuleDialog fragment);
}