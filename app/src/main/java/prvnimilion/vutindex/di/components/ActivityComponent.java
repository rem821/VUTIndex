package prvnimilion.vutindex.di.components;

import dagger.Subcomponent;
import prvnimilion.vutindex.di.PerActivity;
import prvnimilion.vutindex.di.modul.ActivityModule;
import prvnimilion.vutindex.ui.activities.AboutActivity;
import prvnimilion.vutindex.ui.activities.DashboardActivity;
import prvnimilion.vutindex.ui.activities.IndexActivity;
import prvnimilion.vutindex.ui.activities.MainActivity;
import prvnimilion.vutindex.ui.activities.SettingsActivity;
import prvnimilion.vutindex.ui.activities.SplashScreenActivity;
import prvnimilion.vutindex.ui.activities.WebViewActivity;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

  void inject(VutIndexBaseActivity act);

  void inject(SplashScreenActivity act);

  void inject(MainActivity act);

  void inject(IndexActivity act);

  void inject(DashboardActivity act);

  void inject(SettingsActivity act);

  void inject(AboutActivity act);

  void inject(WebViewActivity act);
}