package prvnimilion.vutindex.di.components;

import dagger.Component;
import prvnimilion.vutindex.di.ConfigPersistent;
import prvnimilion.vutindex.di.modul.ActivityModule;
import prvnimilion.vutindex.di.modul.FragmentModule;

/**
 * Created by stand on 23.08.2017.
 */

@ConfigPersistent
@Component(dependencies = ApplicationComponent.class)
public interface ConfigPersistentComponent {
  ActivityComponent activityComponent(ActivityModule activityModule);

  FragmentComponent fragmentComponent(FragmentModule fragmentModule);

}
