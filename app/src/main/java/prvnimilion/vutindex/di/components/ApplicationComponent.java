package prvnimilion.vutindex.di.components;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import prvnimilion.vutindex.VutIndexApplication;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.di.modul.ApplicationModule;
import prvnimilion.vutindex.services.IndexService;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.tools.VutLogin;

/**
 * Created by stand on 23.08.2017.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
  void inject(VutIndexApplication app);

  @ApplicationContext
  Context context();

  Application application();

  DbOpenHelper dbHelper();

  RxEventBus eventBus();

  IndexService indexService();

  VutLogin vutLogin();

}
