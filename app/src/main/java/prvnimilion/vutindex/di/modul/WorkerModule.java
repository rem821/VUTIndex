package prvnimilion.vutindex.di.modul;

import androidx.work.Worker;
import dagger.Module;
import dagger.Provides;

@Module
public class WorkerModule {
  private final Worker mWorker;

  public WorkerModule(Worker worker) {
    mWorker = worker;
  }

  @Provides
  Worker provideWorker() {
    return mWorker;
  }
}
