package prvnimilion.vutindex.di.modul;

import android.app.Activity;
import android.content.Context;

import javax.inject.Named;

import androidx.fragment.app.FragmentManager;
import dagger.Module;
import dagger.Provides;
import prvnimilion.vutindex.di.ActivityContext;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;
import prvnimilion.vutindex.ui.activities.WebViewActivity;
import prvnimilion.vutindex.ui.activities.base.VutIndexBaseActivity;

import static prvnimilion.vutindex.ui.activities.WebViewActivity.EXTRA_WEBVIEW_URL;


@Module
public class ActivityModule {

  private final Activity mActivity;

  public ActivityModule(Activity activity) {
    mActivity = activity;
  }

  @Provides
  ObservableLifeCycleManager provideViewLifeCycleManager() {
    return ((ObservableLifeCycleManager.IObservableLifeCycleManager) mActivity).getObservableLifeCycleManager();
  }

  @Provides
  Activity provideActivity() {
    return mActivity;
  }

  @Provides
  FragmentManager providesSupportFragmentManager() {
    return ((VutIndexBaseActivity) mActivity).getSupportFragmentManager();
  }

  @Provides
  @ActivityContext
  Context providesContext() {
    return mActivity;
  }

  @Provides
  @Named(EXTRA_WEBVIEW_URL)
  String provideWebViewActivityUrl() {
    if (mActivity instanceof WebViewActivity) {
      return WebViewActivity.getUrl(mActivity.getIntent());
    }
    throw new RuntimeException("Activity not instance of WebViewActivity - cannot provide EXTRA_WEBVIEW_URL");
  }
}