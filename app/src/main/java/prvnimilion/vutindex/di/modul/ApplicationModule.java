package prvnimilion.vutindex.di.modul;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.services.IndexService;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.tools.VutLogin;

@Module
public class ApplicationModule {
  private final Application mApplication;
  private final RxEventBus eventBus;
  private final Logger logger;
  private final DbOpenHelper dbOpenHelper;
  private final IndexService indexService;
  private final VutLogin vutLogin;

  public ApplicationModule(Application application) {
    this.logger = new Logger(application);
    eventBus = new RxEventBus();
    mApplication = application;
    dbOpenHelper = new DbOpenHelper(application);
    indexService = new IndexService(dbOpenHelper);
    vutLogin = new VutLogin(application.getApplicationContext(), eventBus);
  }

  @Provides
  Application provideApplication() {
    return mApplication;
  }

  @Provides
  @ApplicationContext
  Context provideContext() {
    return mApplication;
  }

  @Provides
  RxEventBus providesRxEventBus() {
    return eventBus;
  }

  @Provides
  @Singleton
  Logger providerLogger() {
    return logger;
  }

  @Provides
  @Singleton
  DbOpenHelper provideDbOpenHelper() {
    return dbOpenHelper;
  }

  @Provides
  @Singleton
  IndexService provideIndexService() {
    return indexService;
  }

  @Provides
  @Singleton
  VutLogin provideVutLogin() {
    return vutLogin;
  }

}