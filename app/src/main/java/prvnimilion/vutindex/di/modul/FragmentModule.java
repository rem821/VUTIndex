package prvnimilion.vutindex.di.modul;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import dagger.Module;
import dagger.Provides;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;


@Module
public class FragmentModule {
  private final Fragment mFragment;

  public FragmentModule(Fragment fragment) {
    mFragment = fragment;
  }

  @Provides
  Fragment provideFragment() {
    return mFragment;
  }

  @Provides
  FragmentManager providesSupportFragmentManager() {
    return mFragment.getActivity().getSupportFragmentManager();
  }

  @Provides
  ObservableLifeCycleManager provideViewLifeCycleManager() {
    return ((ObservableLifeCycleManager.IObservableLifeCycleManager) mFragment).getObservableLifeCycleManager();
  }


	/*
    @Nullable
	@Provides
	@Named(EXTRA_FRAGMENT_PRICE_DIFFERENCES_KUNNR)
	String providesFragmentPriceDifferencesKunnr()
	{
		if (mFragment instanceof OrdersPriceDifferencesListFragment)
		{
			return OrdersPriceDifferencesListFragment.getKunnr(mFragment.getArguments());
		}
		throw new RuntimeException("Fragment not instance of OrdersPriceDifferencesListFragment - cannot provide EXTRA_FRAGMENT_PRICE_DIFFERENCES_KUNNR");
	}
	*/

}