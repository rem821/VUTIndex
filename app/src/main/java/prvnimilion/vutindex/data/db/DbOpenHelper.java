package prvnimilion.vutindex.data.db;

/**
 * Created by stand on 23.08.2017.
 */

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import javax.inject.Inject;
import javax.inject.Singleton;

import prvnimilion.vutindex.BuildConfig;
import prvnimilion.vutindex.data.db.dataClasses.IT_INDEXDB;
import prvnimilion.vutindex.data.db.dataClasses.IT_SEMESTERDB;
import prvnimilion.vutindex.data.db.dataClasses.IT_SUBJECTDB;
import prvnimilion.vutindex.di.ApplicationContext;

@Singleton
public class DbOpenHelper extends OrmLiteSqliteOpenHelper {

  //region DAOS
  Dao<IT_INDEXDB, String> IT_INDEXDBDao = null;
  Dao<IT_SEMESTERDB, String> IT_SEMESTERDBDao = null;
  Dao<IT_SUBJECTDB, String> IT_SUBJECTDBDao = null;
  //endregion


  @Inject
  public DbOpenHelper(@ApplicationContext Context context) {
    super(context, BuildConfig.DBName, null, BuildConfig.DBVersion);
    this.setWriteAheadLoggingEnabled(true);
  }


  public void closeSingleton() {
    this.close();
    DaoManager.clearCache();
    DaoManager.clearDaoCache();
  }

  @Override
  public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
    try {
      createAllTables(arg1);
    } catch (SQLException e) {
      throw e;
    } catch (java.sql.SQLException e) {
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2, int arg3) {
    try {
      dropAllTables(arg1);
      onCreate(arg0, arg1);
    } catch (SQLException e) {
      throw e;
    } catch (java.sql.SQLException e) {
    }
  }

  public void dropAllTables(ConnectionSource arg1) throws java.sql.SQLException {
    TableUtils.dropTable(arg1, IT_INDEXDB.class, true);
    TableUtils.dropTable(arg1, IT_SEMESTERDB.class, true);
    TableUtils.dropTable(arg1, IT_SUBJECTDB.class, true);

  }

  private void createAllTables(ConnectionSource arg1) throws java.sql.SQLException {
    TableUtils.createTable(arg1, IT_INDEXDB.class);
    TableUtils.createTable(arg1, IT_SEMESTERDB.class);
    TableUtils.createTable(arg1, IT_SUBJECTDB.class);

  }

  public void recreateAllTables() throws java.sql.SQLException {
    ConnectionSource connectionSource = getConnectionSource();
    dropAllTables(connectionSource);
    createAllTables(connectionSource);
  }

  @Override
  public void close() {
    IT_INDEXDBDao = null;
    IT_SEMESTERDBDao = null;
    IT_SUBJECTDBDao = null;
  }


  public Dao<IT_INDEXDB, String> getIT_INDEXDBDao() throws java.sql.SQLException {
    if (IT_INDEXDBDao == null) {
      IT_INDEXDBDao = DaoManager.createDao(getConnectionSource(), IT_INDEXDB.class);
    }
    return IT_INDEXDBDao;
  }

  public Dao<IT_SEMESTERDB, String> getIT_SEMESTERDBDao() throws java.sql.SQLException {
    if (IT_SEMESTERDBDao == null) {
      IT_SEMESTERDBDao = DaoManager.createDao(getConnectionSource(), IT_SEMESTERDB.class);
    }
    return IT_SEMESTERDBDao;
  }

  public Dao<IT_SUBJECTDB, String> getIT_SUBJECTDBDao() throws java.sql.SQLException {
    if (IT_SUBJECTDBDao == null) {
      IT_SUBJECTDBDao = DaoManager.createDao(getConnectionSource(), IT_SUBJECTDB.class);
    }
    return IT_SUBJECTDBDao;
  }


}
