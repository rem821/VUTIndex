package prvnimilion.vutindex.data.db.dataClasses;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;


/**
 * Created by stand on 25.08.2017.
 */
@DatabaseTable(tableName = IT_SUBJECTDB.TABLE_NAME)
public class IT_SUBJECTDB {

  public static final String TABLE_NAME = "IT_SUBJECTDB";
  public static final String TABLE_DESCRIPTION = "Předmět žákova semestru";

  public static final String TABLE_ID_COLUMN = "IT_SUBJECTDB_TABLE_ID";
  public static final String SUBJECT_ID_COLUMN = "IT_SUBJECTDB_SUBJECT_ID";
  public static final String SUBJECT_FULL_NAME_COLUMN = "IT_SUBJECTDB_SUBJECT_FULL_NAME";
  public static final String SUBJECT_SHORT_NAME_COLUMN = "IT_SUBJECTDB_SUBJECT_SHORT_NAME";
  public static final String TYPE_COLUMN = "IT_SUBJECTDB_TYPE";
  public static final String CREDITS_COLUMN = "IT_SUBJECTDB_CREDITS";
  public static final String COMPLETION_COLUMN = "IT_SUBJECTDB_COMPLETION";
  public static final String CREDIT_GIVEN_COLUMN = "IT_SUBJECTDB_CREDIT_GIVEN";
  public static final String POINTS_COLUMN = "IT_SUBJECTDB_POINTS";
  public static final String GRADE_COLUMN = "IT_SUBJECTDB_GRADE";
  public static final String TERM_TIME_COLUMN = "IT_SUBJECTDB_TERM_TIME";
  public static final String PASSED_COLUMN = "IT_SUBJECTDB_PASSED";
  public static final String VSP_COLUMN = "IT_SUBJECTDB_VSP";

  @DatabaseField(generatedId = true, columnName = TABLE_ID_COLUMN)
  UUID SUBJECT_ID;

  @DatabaseField(canBeNull = false, columnName = SUBJECT_ID_COLUMN)
  UUID ID;

  @DatabaseField(columnName = SUBJECT_FULL_NAME_COLUMN)
  String SUBJECT_FULL_NAME;

  @DatabaseField(columnName = SUBJECT_SHORT_NAME_COLUMN)
  String SUBJECT_SHORT_NAME;

  @DatabaseField(columnName = TYPE_COLUMN)
  String TYPE;

  @DatabaseField(columnName = CREDITS_COLUMN)
  String CREDITS;

  @DatabaseField(columnName = COMPLETION_COLUMN)
  String COMPLETION;

  @DatabaseField(columnName = CREDIT_GIVEN_COLUMN)
  String CREDIT_GIVEN;

  @DatabaseField(columnName = POINTS_COLUMN)
  String POINTS;

  @DatabaseField(columnName = GRADE_COLUMN)
  String GRADE;

  @DatabaseField(columnName = TERM_TIME_COLUMN)
  String TERM_TIME;

  @DatabaseField(columnName = PASSED_COLUMN)
  String PASSED;

  @DatabaseField(columnName = VSP_COLUMN)
  String VSP;

  public IT_SUBJECTDB() {
  }

  public IT_SUBJECTDB(UUID ID) {
    this.ID = ID;
  }

  public IT_SUBJECTDB(UUID ID, String SUBJECT_FULL_NAME, String SUBJECT_SHORT_NAME, String TYPE, String CREDITS, String COMPLETION, String CREDIT_GIVEN, String POINTS, String GRADE, String TERM_TIME, String PASSED, String VSP) {
    this.ID = ID;
    this.SUBJECT_FULL_NAME = SUBJECT_FULL_NAME;
    this.SUBJECT_SHORT_NAME = SUBJECT_SHORT_NAME;
    this.TYPE = TYPE;
    this.CREDITS = CREDITS;
    this.COMPLETION = COMPLETION;
    this.CREDIT_GIVEN = CREDIT_GIVEN;
    this.POINTS = POINTS;
    this.GRADE = GRADE;
    this.TERM_TIME = TERM_TIME;
    this.PASSED = PASSED;
    this.VSP = VSP;
  }

  public UUID getID() {
    return ID;
  }

  public String getSUBJECT_FULL_NAME() {
    return SUBJECT_FULL_NAME;
  }

  public void setSUBJECT_FULL_NAME(String SUBJECT_FULL_NAME) {
    this.SUBJECT_FULL_NAME = SUBJECT_FULL_NAME;
  }

  public String getSUBJECT_SHORT_NAME() {
    return SUBJECT_SHORT_NAME;
  }

  public void setSUBJECT_SHORT_NAME(String SUBJECT_SHORT_NAME) {
    this.SUBJECT_SHORT_NAME = SUBJECT_SHORT_NAME;
  }

  public String getTYPE() {
    return TYPE;
  }

  public void setTYPE(String TYPE) {
    this.TYPE = TYPE;
  }

  public String getCREDITS() {
    return CREDITS;
  }

  public void setCREDITS(String CREDITS) {
    this.CREDITS = CREDITS;
  }

  public String getCOMPLETION() {
    return COMPLETION;
  }

  public void setCOMPLETION(String COMPLETION) {
    this.COMPLETION = COMPLETION;
  }

  public String getCREDIT_GIVEN() {
    return CREDIT_GIVEN;
  }

  public void setCREDIT_GIVEN(String CREDIT_GIVEN) {
    this.CREDIT_GIVEN = CREDIT_GIVEN;
  }

  public String getPOINTS() {
    return POINTS;
  }

  public void setPOINTS(String POINTS) {
    this.POINTS = POINTS;
  }

  public String getGRADE() {
    return GRADE;
  }

  public void setGRADE(String GRADE) {
    this.GRADE = GRADE;
  }

  public String getTERM_TIME() {
    return TERM_TIME;
  }

  public void setTERM_TIME(String TERM_TIME) {
    this.TERM_TIME = TERM_TIME;
  }

  public String getPASSED() {
    return PASSED;
  }

  public void setPASSED(String PASSED) {
    this.PASSED = PASSED;
  }

  public String getVSP() {
    return VSP;
  }

  public void setVSP(String VSP) {
    this.VSP = VSP;
  }
}