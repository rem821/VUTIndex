package prvnimilion.vutindex.data.db.dataClasses;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;


/**
 * Created by stand on 25.08.2017.
 */
@DatabaseTable(tableName = IT_INDEXDB.TABLE_NAME)
public class IT_INDEXDB {

  public static final String TABLE_NAME = "IT_INDEXDB";
  public static final String TABLE_DESCRIPTION = "Žákův index";

  public static final String TABLE_ID_COLUMN = "IT_INDEXDB_TABLE_ID";
  public static final String INDEX_ID_COLUMN = "IT_INDEXDB_INDEX_ID";
  public static final String SEMESTERS_COUNT_COLUMN = "IT_INDEXDB_SEMESTERS_COUNT";


  @DatabaseField(generatedId = true, columnName = TABLE_ID_COLUMN)
  UUID INDEX_ID;

  @DatabaseField(canBeNull = false, columnName = SEMESTERS_COUNT_COLUMN)
  int SEMESTERS_COUNT;

  public IT_INDEXDB() {
  }

  public IT_INDEXDB(int SEMESTERS_COUNT) {
    this.SEMESTERS_COUNT = SEMESTERS_COUNT;
  }

  public UUID getINDEX_ID() {
    return INDEX_ID;
  }

  public int getSEMESTERS_COUNT() {
    return SEMESTERS_COUNT;
  }

  public void setSEMESTERS_COUNT(int SEMESTERS_COUNT) {
    this.SEMESTERS_COUNT = SEMESTERS_COUNT;
  }
}