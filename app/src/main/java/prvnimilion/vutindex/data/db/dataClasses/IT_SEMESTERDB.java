package prvnimilion.vutindex.data.db.dataClasses;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;


/**
 * Created by stand on 25.08.2017.
 */
@DatabaseTable(tableName = IT_SEMESTERDB.TABLE_NAME)
public class IT_SEMESTERDB {

  public static final String TABLE_NAME = "IT_SEMESTERDB";
  public static final String TABLE_DESCRIPTION = "Semestr žákova indexu";

  public static final String TABLE_ID_COLUMN = "IT_SEMESTERDB_TABLE_ID";
  public static final String SEMESTER_ID_COLUMN = "IT_SEMESTERDB_SEMESTER_ID";
  public static final String SUBJECTS_COUNT_COLUMN = "IT_SEMESTERDB_SUBJECT_COUNT";


  @DatabaseField(canBeNull = false, columnName = TABLE_ID_COLUMN)
  UUID ID;

  @DatabaseField(generatedId = true, columnName = SEMESTER_ID_COLUMN)
  UUID SEMESTER_ID;

  @DatabaseField(canBeNull = false, columnName = SUBJECTS_COUNT_COLUMN)
  int SUBJECTS_COUNT;

  public IT_SEMESTERDB() {
  }

  public IT_SEMESTERDB(UUID ID, int SUBJECTS_COUNT) {
    this.ID = ID;
    this.SUBJECTS_COUNT = SUBJECTS_COUNT;
  }

  public UUID getID() {
    return ID;
  }

  public void setID(UUID ID) {
    this.ID = ID;
  }

  public UUID getSEMESTER_ID() {
    return SEMESTER_ID;
  }

  public int getSUBJECTS_COUNT() {
    return SUBJECTS_COUNT;
  }

  public void setSUBJECTS_COUNT(int SUBJECTS_COUNT) {
    this.SUBJECTS_COUNT = SUBJECTS_COUNT;
  }
}