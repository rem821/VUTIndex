package prvnimilion.vutindex.mvp.views;

import android.app.Activity;
import android.content.Context;

import com.evrencoskun.tableview.TableView;

import prvnimilion.vutindex.mvp.base.MvpView;

/**
 * Created by stand on 12.08.2017.
 */

public interface IndexActivityMvpView extends MvpView {

  Activity getActivity();

  Context getContext();

  TableView getTableView();

  void showToast(String content);

  void showLoadingPanel(boolean show);

  void toggleImmersive(boolean on);

}
