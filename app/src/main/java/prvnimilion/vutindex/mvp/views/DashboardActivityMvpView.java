package prvnimilion.vutindex.mvp.views;

import android.app.Activity;
import android.content.Context;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;
import prvnimilion.vutindex.mvp.base.MvpView;

/**
 * Created by stand on 12.08.2017.
 */

public interface DashboardActivityMvpView extends MvpView {

  Activity getActivity();

  Context getContext();

  void showToast(String content);

  void showDonationsDialog();

  NavigationView getNvDrawerMenu();

  DrawerLayout getDrawerLayout();

}
