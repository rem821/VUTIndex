package prvnimilion.vutindex.mvp.views;

import android.app.Activity;
import android.content.Context;
import android.widget.SeekBar;
import android.widget.TextView;

import prvnimilion.vutindex.mvp.base.MvpView;

/**
 * Created by stand on 12.08.2017.
 */

public interface SettingsActivityMvpView extends MvpView {

  Activity getActivity();

  Context getContext();

  SeekBar getSeekbar();

  TextView getSeekbarCaption();

  void showStartupModuleDialog();

  void showToast(String content);

}
