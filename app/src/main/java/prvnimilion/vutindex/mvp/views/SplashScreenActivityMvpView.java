package prvnimilion.vutindex.mvp.views;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import prvnimilion.vutindex.mvp.base.MvpView;

/**
 * Created by stand on 12.08.2017.
 */

public interface SplashScreenActivityMvpView extends MvpView {

  Activity getActivity();

  Context getContext();

  View getLayout();

  void showToast(String content);
}
