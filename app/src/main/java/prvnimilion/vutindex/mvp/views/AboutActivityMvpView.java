package prvnimilion.vutindex.mvp.views;

import android.app.Activity;
import android.content.Context;

import prvnimilion.vutindex.mvp.base.MvpView;

/**
 * Created by stand on 12.08.2017.
 */

public interface AboutActivityMvpView extends MvpView {

  Activity getActivity();

  Context getContext();

  void showToast(String content);

}
