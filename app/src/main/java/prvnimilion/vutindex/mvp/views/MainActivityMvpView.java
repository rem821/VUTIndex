package prvnimilion.vutindex.mvp.views;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import prvnimilion.vutindex.mvp.base.MvpView;

/**
 * Created by stand on 12.08.2017.
 */

public interface MainActivityMvpView extends MvpView {

  Activity getActivity();

  Context getContext();

  ImageView getIcon();

  void showToast(String content);

  void showLoadingPanel(boolean show);

  View getLayout();
}
