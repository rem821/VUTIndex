package prvnimilion.vutindex.mvp.views;

import android.app.Activity;
import android.view.View;

import prvnimilion.vutindex.mvp.base.MvpView;


/**
 * Created by stand on 12.08.2017.
 */

public interface WebViewMvpView extends MvpView {

  Activity getActivity();

  void showToast(String content);

  View getWebView();

  void showLoadingPanel(boolean show);

}
