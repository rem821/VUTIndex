package prvnimilion.vutindex.mvp.models.IndexTableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.ITableView;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractSorterViewHolder;

import prvnimilion.vutindex.R;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.ColumnHeader;

public class ColumnHeaderViewHolder extends AbstractSorterViewHolder {

  private static final String LOG_TAG = ColumnHeaderViewHolder.class.getSimpleName();

  public final LinearLayout column_header_container;
  public final TextView column_header_textview;
  public final ITableView tableView;

  public ColumnHeaderViewHolder(View itemView, ITableView tableView) {
    super(itemView);
    this.tableView = tableView;
    column_header_textview = itemView.findViewById(R.id.column_header_textView);
    column_header_container = itemView.findViewById(R.id.column_header_container);
  }

  public void setColumnHeader(ColumnHeader columnHeader) {
    column_header_textview.setText(String.valueOf(columnHeader.getData()));

    // If your TableView should have auto resize for cells & columns.
    // Then you should consider the below lines. Otherwise, you can remove them.

    // It is necessary to remeasure itself.
    column_header_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;
    column_header_textview.requestLayout();
  }
}
