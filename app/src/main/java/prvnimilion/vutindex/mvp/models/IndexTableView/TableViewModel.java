package prvnimilion.vutindex.mvp.models.IndexTableView;

import android.content.Context;

public class TableViewModel {

  // Columns indexes
  public static final int CHECK_CELL_TYPE = 1;

  // Constant values for icons
  public static final int PASSED = 1;

  private Context mContext;

  public TableViewModel(Context context) {
    mContext = context;
  }

  public int getCellItemViewType(int column) {

    switch (column) {
      case 4:
        // 5. column header is CREDIT.
        return CHECK_CELL_TYPE;
      case 8:
        // 8. column header is PASS.
        return CHECK_CELL_TYPE;
      case 9:
        // 8. column header is VSP.
        return CHECK_CELL_TYPE;
      default:
        return 0;
    }
  }
}
