package prvnimilion.vutindex.mvp.models.IndexTableView;

import android.content.Context;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TableViewListener implements ITableViewListener {

  private Toast mToast;
  private Context mContext;
  private TableView mTableView;

  public TableViewListener(TableView tableView) {
    this.mContext = tableView.getContext();
    this.mTableView = tableView;
  }

  /**
   * Called when user click any cell item.
   *
   * @param cellView : Clicked Cell ViewHolder.
   * @param column   : X (Column) position of Clicked Cell item.
   * @param row      : Y (Row) position of Clicked Cell item.
   */
  @Override
  public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

  }

  /**
   * Called when user long press any cell item.
   *
   * @param cellView : Long Pressed Cell ViewHolder.
   * @param column   : X (Column) position of Long Pressed Cell item.
   * @param row      : Y (Row) position of Long Pressed Cell item.
   */
  @Override
  public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, final int column, int row) {

  }

  /**
   * Called when user click any column header item.
   *
   * @param columnHeaderView : Clicked Column Header ViewHolder.
   * @param column           : X (Column) position of Clicked Column Header item.
   */
  @Override
  public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

  }

  /**
   * Called when user long press any column header item.
   *
   * @param columnHeaderView : Long Pressed Column Header ViewHolder.
   * @param column           : X (Column) position of Long Pressed Column Header item.
   */
  @Override
  public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

  }

  /**
   * Called when user click any Row Header item.
   *
   * @param rowHeaderView : Clicked Row Header ViewHolder.
   * @param row           : Y (Row) position of Clicked Row Header item.
   */
  @Override
  public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

  }

  /**
   * Called when user long press any row header item.
   *
   * @param rowHeaderView : Long Pressed Row Header ViewHolder.
   * @param row           : Y (Row) position of Long Pressed Row Header item.
   */
  @Override
  public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

  }
}
