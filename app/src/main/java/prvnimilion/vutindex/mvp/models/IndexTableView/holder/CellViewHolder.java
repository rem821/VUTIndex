package prvnimilion.vutindex.mvp.models.IndexTableView.holder;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import prvnimilion.vutindex.R;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.Cell;

public class CellViewHolder extends AbstractViewHolder {

  public final TextView cell_textview;
  public final LinearLayout cell_container;
  private Cell cell;
  private Context context;

  public CellViewHolder(View itemView, Context context) {
    super(itemView);
    this.context = context;
    cell_textview = itemView.findViewById(R.id.cell_data);
    cell_container = itemView.findViewById(R.id.cell_container);
  }

  public void setCell(Cell cell) {
    this.cell = cell;
    cell_textview.setText(String.valueOf(cell.getData()));
    cell_container.setBackgroundColor(context.getResources().getColor(R.color.grayColor));

    // It is necessary to remeasure itself.
    cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;
    cell_textview.requestLayout();
  }
}