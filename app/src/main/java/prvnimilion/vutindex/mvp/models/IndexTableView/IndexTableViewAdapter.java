package prvnimilion.vutindex.mvp.models.IndexTableView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import java.util.List;

import prvnimilion.vutindex.R;
import prvnimilion.vutindex.mvp.models.IndexTableView.holder.CellViewHolder;
import prvnimilion.vutindex.mvp.models.IndexTableView.holder.CheckCellViewHolder;
import prvnimilion.vutindex.mvp.models.IndexTableView.holder.ColumnHeaderViewHolder;
import prvnimilion.vutindex.mvp.models.IndexTableView.holder.RowHeaderViewHolder;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.Cell;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.ColumnHeader;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.RowHeader;

public class IndexTableViewAdapter extends AbstractTableAdapter<ColumnHeader, RowHeader, Cell> {

  private final LayoutInflater mInflater;
  private TableViewModel mTableViewModel;
  private Context context;

  public IndexTableViewAdapter(Context context, TableViewModel tableViewModel) {
    super(context);
    this.context = context;
    this.mTableViewModel = tableViewModel;
    this.mInflater = LayoutInflater.from(mContext);
  }

  @Override
  public AbstractViewHolder onCreateCellViewHolder(ViewGroup parent, int viewType) {
    View layout;

    switch (viewType) {
      case TableViewModel.CHECK_CELL_TYPE:
        layout = mInflater.inflate(R.layout.table_view_image_cell_layout, parent, false);

        return new CheckCellViewHolder(layout);
      default:
        layout = mInflater.inflate(R.layout.table_view_cell_layout, parent, false);

        return new CellViewHolder(layout, context);
    }
  }

  @Override
  public void onBindCellViewHolder(AbstractViewHolder holder, Object cellItemModel, int columnPosition, int rowPosition) {
    Cell cell = (Cell) cellItemModel;


    if (holder instanceof CellViewHolder) {
      ((CellViewHolder) holder).setCell(cell);
    } else if (holder instanceof CheckCellViewHolder) {
      ((CheckCellViewHolder) holder).setCell(cell);
    }
  }

  @Override
  public AbstractViewHolder onCreateColumnHeaderViewHolder(ViewGroup parent, int viewType) {
    View layout = mInflater.inflate(R.layout.table_view_column_header_layout, parent, false);
    return new ColumnHeaderViewHolder(layout, getTableView());
  }

  @Override
  public void onBindColumnHeaderViewHolder(AbstractViewHolder holder, Object columnHeaderItemModel, int columnPosition) {
    ColumnHeader columnHeader = (ColumnHeader) columnHeaderItemModel;

    ColumnHeaderViewHolder columnHeaderViewHolder = (ColumnHeaderViewHolder) holder;
    columnHeaderViewHolder.setColumnHeader(columnHeader);
  }

  @Override
  public AbstractViewHolder onCreateRowHeaderViewHolder(ViewGroup parent, int viewType) {
    View layout = mInflater.inflate(R.layout.table_view_row_header_layout, parent, false);
    return new RowHeaderViewHolder(layout);
  }

  @Override
  public void onBindRowHeaderViewHolder(AbstractViewHolder holder, Object rowHeaderItemModel, int rowPosition) {
    RowHeader rowHeader = (RowHeader) rowHeaderItemModel;

    RowHeaderViewHolder rowHeaderViewHolder = (RowHeaderViewHolder) holder;
    rowHeaderViewHolder.row_header_textview.setText(String.valueOf(rowHeader.getData()));
  }

  @Override
  public View onCreateCornerView() {
    View corner = mInflater.inflate(R.layout.table_view_corner_layout, null);

    return corner;
  }

  @Override
  public int getColumnHeaderItemViewType(int position) {
    return 0;
  }

  @Override
  public int getRowHeaderItemViewType(int position) {
    return 0;
  }

  @Override
  public int getCellItemViewType(int position) {
    return mTableViewModel.getCellItemViewType(position);
  }

  @Override
  public void setAllItems(List<ColumnHeader> columnHeaderItems, List<RowHeader> rowHeaderItems, List<List<Cell>> cellItems) {
    super.setAllItems(columnHeaderItems, rowHeaderItems, cellItems);
  }
}
