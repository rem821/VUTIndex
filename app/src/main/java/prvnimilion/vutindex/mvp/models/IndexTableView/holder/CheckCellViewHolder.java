package prvnimilion.vutindex.mvp.models.IndexTableView.holder;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import androidx.core.content.ContextCompat;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.Cell;

public class CheckCellViewHolder extends AbstractViewHolder {

  public final LinearLayout cell_container;
  public final ImageView cell_image;

  public CheckCellViewHolder(View itemView) {
    super(itemView);
    cell_container = itemView.findViewById(R.id.cell_container);
    cell_image = itemView.findViewById(R.id.cell_image);
  }


  public void setCell(Cell cell) {
    Drawable checkDrawable;
    if (String.valueOf(cell.getData()).matches("")) {
      checkDrawable = ContextCompat.getDrawable(itemView.getContext(), R.drawable.placeholder);
    } else {
      boolean checked = Boolean.parseBoolean(String.valueOf(cell.getData()));
      checkDrawable = ContextCompat.getDrawable(itemView.getContext(), checked ? R.drawable.check : R.drawable.cross);
    }

    cell_image.setImageDrawable(checkDrawable);
  }
}