package prvnimilion.vutindex.mvp.presenters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.databinding.ObservableField;
import androidx.drawerlayout.widget.DrawerLayout;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.mvp.base.BasePresenter;
import prvnimilion.vutindex.mvp.views.DashboardActivityMvpView;
import prvnimilion.vutindex.pojo.SharedPrefsEnums;
import prvnimilion.vutindex.pojo.messages.DonationMessage;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.tools.WorkerManager;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;
import prvnimilion.vutindex.ui.activities.AboutActivity;
import prvnimilion.vutindex.ui.activities.IndexActivity;
import prvnimilion.vutindex.ui.activities.MainActivity;
import prvnimilion.vutindex.ui.activities.SettingsActivity;
import prvnimilion.vutindex.ui.activities.WebViewActivity;

public class DashboardPresenter extends BasePresenter<DashboardActivityMvpView> implements PurchasesUpdatedListener {

  public ObservableField<String> logedInTime = new ObservableField<>();
  public ActionBarDrawerToggle drawerToggle;
  private Context appCntx;
  private ObservableLifeCycleManager observableLifeCycleManager;
  private Logger logger;
  private RxEventBus eventBus;
  private SharedPreferences prefs;
  private CountDownTimer timer;
  private DbOpenHelper dbOpenHelper;
  private BillingClient mBillingClient;
  private NavigationView navigationView;
  private DrawerLayout drawerLayout;

  private String small_donation, medium_donation, big_donation;

  @Inject
  public DashboardPresenter(@ApplicationContext Context appContext,
                            ObservableLifeCycleManager observableLifeCycleManager,
                            Logger logger,
                            DbOpenHelper db,
                            RxEventBus eventBus) {
    this.dbOpenHelper = db;
    this.appCntx = appContext;
    this.observableLifeCycleManager = observableLifeCycleManager;
    this.logger = logger;
    this.eventBus = eventBus;
  }

  @Override
  public void attachView(DashboardActivityMvpView mvpView, boolean isNewlyCreated) {
    super.attachView(mvpView, isNewlyCreated);
    init();
  }

  private void init() {
    initEventBus();

    prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
    WorkerManager.stopService(appCntx);
    if (prefs.getBoolean(SharedPrefsEnums.slowAlarm.toString(), false)) {
      WorkerManager.startService(appCntx, prefs.getInt(SharedPrefsEnums.syncTime.toString(), 15) * 60 * 1000);
    }
    handleDrawerMenu();
    initBillingClient();
  }

  private void handleDrawerMenu() {
    navigationView = getMvpView().getNvDrawerMenu();
    drawerLayout = getMvpView().getDrawerLayout();

    drawerToggle = new ActionBarDrawerToggle(getMvpView().getActivity(), drawerLayout, R.string.drawer_open, R.string.drawer_close) {

      public void onDrawerClosed(View view) {
        super.onDrawerClosed(view);
        for (int i = 0; i < getMvpView().getNvDrawerMenu().getMenu().size(); i++) {
          getMvpView().getNvDrawerMenu().getMenu().getItem(i).setChecked(false);
        }
      }

      public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);
      }
    };

    drawerLayout.setDrawerListener(drawerToggle);

    navigationView.setNavigationItemSelectedListener(menuItem -> {
      menuItem.setChecked(true);
      drawerLayout.closeDrawers();

      String[] buttons = appCntx.getResources().getStringArray(R.array.navigation_drawer_items);
      if (menuItem.getTitle().equals(buttons[0])) {
        openIndex(null);
      } else if (menuItem.getTitle().equals(buttons[1])) {
        openSystem(null);
      } else if (menuItem.getTitle().equals(buttons[2])) {
        openSettings(null);
      } else if (menuItem.getTitle().equals(buttons[3])) {
        openDonations(null);
      } else if (menuItem.getTitle().equals(buttons[4])) {
        openAboutUs(null);
      } else if (menuItem.getTitle().equals(buttons[5])) {
        logOut();
      }
      return true;
    });
  }

  public void openIndex(View view) {
    Intent intent = new Intent(getMvpView().getActivity(), IndexActivity.class);
    getMvpView().getActivity().startActivity(intent);
  }

  public void openSystem(View view) {
    Intent intent = WebViewActivity.createStartIntent(getMvpView().getActivity(), "https://www.vutbr.cz/login/studis");
    getMvpView().getActivity().startActivity(intent);
  }

  public void openSettings(View view) {
    Intent intent = new Intent(getMvpView().getActivity(), SettingsActivity.class);
    getMvpView().getActivity().startActivity(intent);
  }

  public void openAboutUs(View view) {
    Intent intent = new Intent(getMvpView().getActivity(), AboutActivity.class);
    getMvpView().getActivity().startActivity(intent);
  }

  public void openDonations(View view) {
    getMvpView().showDonationsDialog();
  }

  private void initEventBus() {
    this.observableLifeCycleManager.addObservableAndStartObserve("DonationMessage", eventBus.filteredObservable(DonationMessage.class), new Observer<DonationMessage>() {
      @Override
      public void onSubscribe(Disposable d) {

      }

      @Override
      public void onNext(DonationMessage value) {
        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSku(value.getSkuId())
                .setType(BillingClient.SkuType.INAPP)
                .build();
        int responseCode = mBillingClient.launchBillingFlow(getMvpView().getActivity(), flowParams);
        Log.d("BILLINGdebug", "Billing flow response code: " + responseCode);
      }

      @Override
      public void onError(Throwable e) {

      }

      @Override
      public void onComplete() {

      }
    }, true, AndroidSchedulers.mainThread());
  }

  private void initBillingClient() {
    mBillingClient = BillingClient.newBuilder(appCntx).setListener(this).build();
    mBillingClient.startConnection(new BillingClientStateListener() {
      @Override
      public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
        if (billingResponseCode == BillingClient.BillingResponse.OK) {
          Log.d("BILLINGdebug", "Billing setup has finished!");

          mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP, (responseCode, purchasesList) -> {
            if (purchasesList == null) return;
            for (Purchase purchase : purchasesList) {
              mBillingClient.consumeAsync(purchase.getPurchaseToken(), (responseCode1, purchaseToken) -> {
                if (responseCode1 == 0) {
                  Log.d("BILLINGdebug", "Succesfully consumed!");
                }
              });

            }
          });
          List<Purchase> purchases = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP).getPurchasesList();
          if (purchases == null) return;
          for (Purchase purchase : purchases) {
            mBillingClient.consumeAsync(purchase.getPurchaseToken(), (responseCode1, purchaseToken) -> {
              if (responseCode1 == 0) {
                Log.d("BILLINGdebug", "Succesfully consumed!");
              }
            });
          }
        }
      }

      @Override
      public void onBillingServiceDisconnected() {
        Log.d("BILLINGdebug", "Billing service disconnected!");
      }
    });

    List<String> skuList = new ArrayList<>();
    skuList.add("small_donation");
    skuList.add("medium_donation");
    skuList.add("big_donation");
    SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
    params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
    mBillingClient.querySkuDetailsAsync(params.build(), (responseCode, skuDetailsList) -> {
      if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
        for (SkuDetails skuDetails : skuDetailsList) {
          String sku = skuDetails.getSku();
          String price = skuDetails.getPrice();
          if ("small_donation".equals(sku)) {
            small_donation = price;
            Log.d("BILLINGdebug", "small donation price: " + small_donation);
          } else if ("medium_donation".equals(sku)) {
            medium_donation = price;
            Log.d("BILLINGdebug", "medium donation price: " + medium_donation);
          } else if ("big_donation".equals(sku)) {
            big_donation = price;
            Log.d("BILLINGdebug", "big donation price: " + big_donation);
          }
        }
      }
    });
  }

  @Override
  public void onPurchasesUpdated(@BillingClient.BillingResponse int responseCode, List<Purchase> purchases) {
    if (responseCode == BillingClient.BillingResponse.OK && purchases != null) {
      for (Purchase purchase : purchases) {
        Log.d("BILLINGdebug", "purchased: " + purchase.getSku());
        mBillingClient.consumeAsync(purchase.getPurchaseToken(), (responseCode1, purchaseToken) -> {
          Log.d("BILLINGdebug", "purchases and consumed");
        });
      }
    } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
      Log.d("BILLINGdebug", "purchase cancelled");
    } else {
      // Handle any other error codes.
    }
  }

  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_dashboard_action_settings:
        openSettings(null);
        return true;
      case R.id.menu_dashboard_action_support_us:
        openDonations(null);
        return true;
      case R.id.menu_dashboard_action_about:
        openAboutUs(null);
        return true;
      case R.id.menu_dashboard_action_logout:
        logOut();
        return true;
      default:
        return false;
    }
  }

  private void logOut() {
    prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putBoolean(SharedPrefsEnums.logedIn.toString(), false);
    editor.apply();

    WorkerManager.stopService(appCntx);

    Intent intent = new Intent(appCntx, MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    appCntx.startActivity(intent);
    getMvpView().getActivity().finish();
  }
}