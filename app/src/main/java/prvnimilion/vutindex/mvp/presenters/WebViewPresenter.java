package prvnimilion.vutindex.mvp.presenters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.webkit.WebBackForwardList;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Inject;
import javax.inject.Named;

import androidx.core.app.ActivityCompat;
import prvnimilion.vutindex.BuildConfig;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.mvp.base.BasePresenter;
import prvnimilion.vutindex.mvp.views.WebViewMvpView;
import prvnimilion.vutindex.pojo.SharedPrefsEnums;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;

import static android.content.Context.DOWNLOAD_SERVICE;
import static prvnimilion.vutindex.ui.activities.WebViewActivity.EXTRA_WEBVIEW_URL;

/**
 * Created by stand on 12.08.2017.
 */

public class WebViewPresenter extends BasePresenter<WebViewMvpView> {

  WebView webView;
  private String url;
  private Context appCntx;
  private ObservableLifeCycleManager observableLifeCycleManager;
  private Logger logger;
  private DbOpenHelper db;
  private RxEventBus eventBus;

  private FirebaseAnalytics firebaseAnalytics;

  @Inject
  public WebViewPresenter(@Named(EXTRA_WEBVIEW_URL) String url,
                          @ApplicationContext Context appContext,
                          ObservableLifeCycleManager observableLifeCycleManager,
                          Logger logger,
                          DbOpenHelper db,
                          RxEventBus eventBus) {
    this.url = url;
    this.appCntx = appContext;
    this.observableLifeCycleManager = observableLifeCycleManager;
    this.logger = logger;
    this.db = db;
    this.eventBus = eventBus;
  }

  @Override
  public void attachView(WebViewMvpView mvpView, boolean isNewlyCreated) {
    super.attachView(mvpView, isNewlyCreated);
    init();
  }

  private void init() {
    firebaseAnalytics = FirebaseAnalytics.getInstance(appCntx);
    Log.d("VUTdebug", "Začalo načítání webové stránky");
    showWebPage(url);
  }

  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_webview_action_reload:
        reloadPage();
        return true;
      default:
        return false;
    }

  }

  @SuppressLint("SetJavaScriptEnabled")
  private void showWebPage(String url) {
    webView = (WebView) getMvpView().getWebView();
    if (Build.VERSION.SDK_INT >= 19) {
      webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
    } else {
      webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }
    webView.getSettings().setJavaScriptEnabled(true);
    webView.getSettings().setAppCacheEnabled(true);
    webView.getSettings().setDomStorageEnabled(true);
    webView.getSettings().setAllowFileAccess(true);
    webView.getSettings().setBuiltInZoomControls(true);
    webView.getSettings().setDisplayZoomControls(false);

    getMvpView().showLoadingPanel(true);
    webView.loadUrl(url);

    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);

    String username = prefs.getString(SharedPrefsEnums.username.toString(), "");
    String password = prefs.getString(SharedPrefsEnums.password.toString(), "");
    String js = "(function() {" +
            "var form = document.getElementById(\"login_form\");" +
            "var username = form.elements[\"login7\"];" +
            "var password = form.elements[\"passwd7\"];" +
            "username.value = '" + username + "';" +
            "password.value = '" + password + "';" +
            "document.getElementsByName(\"login\")[0].click();" +
            "})();";

    webView.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(webView, url);

        if (Build.VERSION.SDK_INT >= 19) {
          view.evaluateJavascript("javascript:" + js, s -> {

          });
        } else {
          view.loadUrl("javascript:" + js);
        }
        if (url.contains("vutbr.cz/studis") || url.contains("vutbr.cz/intra")) {
          getMvpView().showLoadingPanel(false);
          isStoragePermissionGranted();
        }
      }
    });

    webView.setDownloadListener((url12, userAgent, contentDisposition, mimeType, contentLength) -> {
      if (isStoragePermissionGranted()) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url12));

        request.setMimeType(mimeType);
        //------------------------COOKIE!!------------------------
        String cookies = CookieManager.getInstance().getCookie(url12);
        request.addRequestHeader("cookie", cookies);
        //------------------------COOKIE!!------------------------
        request.addRequestHeader("User-Agent", userAgent);
        request.setDescription("Downloading file...");
        request.setTitle(URLUtil.guessFileName(url12, contentDisposition, mimeType));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url12, contentDisposition, mimeType));
        DownloadManager dm = (DownloadManager) appCntx.getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(request);
        getMvpView().showToast("Downloading file");
      }
    });
  }

  private void reloadPage() {
    webView.reload();
  }

  private boolean isStoragePermissionGranted() {
    if (Build.VERSION.SDK_INT >= 23) {
      if (appCntx.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
              == PackageManager.PERMISSION_GRANTED) {
        Log.d("VUTdebug", "Permission is granted");
        return true;
      } else {
        Log.d("VUTdebug", "Permission is revoked");
        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
          Bundle bundle = new Bundle();
          bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "Downloaded a file!");
          firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, bundle);
        }
        ActivityCompat.requestPermissions(getMvpView().getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        return false;
      }
    } else {
      return true;
    }
  }

  public boolean goBack() {
    if (webView == null) return false;
    if (webView.getUrl().contains("www.vutbr.cz/?armsgt") || getPreviousUrl().contains("www.vutbr.cz/login")) {
      return false;
    }
    if (webView.canGoBack()) {
      webView.goBack();
      return true;
    }
    return false;
  }

  private String getPreviousUrl() {
    String previousUrl = "";
    WebBackForwardList webBackForwardList = webView.copyBackForwardList();
    if (webBackForwardList.getCurrentIndex() > 0) {
      previousUrl = webBackForwardList.getItemAtIndex(webBackForwardList.getCurrentIndex() - 1).getUrl();
    }
    return previousUrl;
  }
}
