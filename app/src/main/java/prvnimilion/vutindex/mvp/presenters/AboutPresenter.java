package prvnimilion.vutindex.mvp.presenters;

import android.content.Context;

import javax.inject.Inject;

import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.mvp.base.BasePresenter;
import prvnimilion.vutindex.mvp.views.AboutActivityMvpView;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;

public class AboutPresenter extends BasePresenter<AboutActivityMvpView> {

  private Context appCntx;
  private ObservableLifeCycleManager observableLifeCycleManager;
  private Logger logger;
  private RxEventBus eventBus;
  private DbOpenHelper dbOpenHelper;

  @Inject
  public AboutPresenter(@ApplicationContext Context appContext,
                        ObservableLifeCycleManager observableLifeCycleManager,
                        Logger logger,
                        DbOpenHelper db,
                        RxEventBus eventBus) {
    this.dbOpenHelper = db;
    this.appCntx = appContext;
    this.observableLifeCycleManager = observableLifeCycleManager;
    this.logger = logger;
    this.eventBus = eventBus;
  }

  @Override
  public void attachView(AboutActivityMvpView mvpView, boolean isNewlyCreated) {
    super.attachView(mvpView, isNewlyCreated);
    init();
  }

  private void init() {

  }

}