package prvnimilion.vutindex.mvp.presenters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

import javax.inject.Inject;

import prvnimilion.vutindex.BuildConfig;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.mvp.base.BasePresenter;
import prvnimilion.vutindex.mvp.views.SplashScreenActivityMvpView;
import prvnimilion.vutindex.pojo.SharedPrefsEnums;
import prvnimilion.vutindex.pojo.StartupModules;
import prvnimilion.vutindex.services.IndexService;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.tools.VutLogin;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;
import prvnimilion.vutindex.ui.activities.DashboardActivity;
import prvnimilion.vutindex.ui.activities.IndexActivity;
import prvnimilion.vutindex.ui.activities.MainActivity;
import prvnimilion.vutindex.ui.activities.WebViewActivity;

public class SplashScreenPresenter extends BasePresenter<SplashScreenActivityMvpView> {

  private Context appCntx;
  private ObservableLifeCycleManager observableLifeCycleManager;
  private Logger logger;
  private RxEventBus eventBus;
  private SharedPreferences prefs;
  private IndexService indexService;
  private VutLogin vutLogin;
  private DbOpenHelper dbOpenHelper;

  private FirebaseRemoteConfig firebaseRemoteConfig;
  private FirebaseAnalytics firebaseAnalytics;

  private boolean loading = false;
  private boolean online = false;

  @Inject
  public SplashScreenPresenter(@ApplicationContext Context appContext,
                               ObservableLifeCycleManager observableLifeCycleManager,
                               Logger logger,
                               DbOpenHelper db,
                               RxEventBus eventBus,
                               IndexService indexService,
                               VutLogin vutLogin) {
    this.dbOpenHelper = db;
    this.appCntx = appContext;
    this.observableLifeCycleManager = observableLifeCycleManager;
    this.logger = logger;
    this.eventBus = eventBus;
    this.indexService = indexService;
    this.vutLogin = vutLogin;
  }

  @Override
  public void attachView(SplashScreenActivityMvpView mvpView, boolean isNewlyCreated) {
    super.attachView(mvpView, isNewlyCreated);
    init();
  }

  private void init() {
    firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
            .setDeveloperModeEnabled(BuildConfig.DEBUG)
            .build();
    firebaseRemoteConfig.setConfigSettings(configSettings);
    firebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
    fetchRemoteConfig();
    firebaseAnalytics = FirebaseAnalytics.getInstance(getMvpView().getActivity());

    prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);

    getMvpView().getLayout().getViewTreeObserver().addOnGlobalLayoutListener(() -> {
      if (loading) return;
      if (prefs.getBoolean(SharedPrefsEnums.logedIn.toString(), false) && prefs.getBoolean(SharedPrefsEnums.rememberCredentials.toString(), false)) {
        if (isOnline()) {
          new LoginAsyncTask().execute();
        }
      } else {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
          @Override
          public void run() {
            Intent intent = new Intent(getMvpView().getActivity(), MainActivity.class);
            getMvpView().getActivity().startActivity(intent);
          }
        }, 2 * 1000);
      }
    });

  }

  private void fetchRemoteConfig() {

    long cacheExpiration = 3600;
    if (firebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
      cacheExpiration = 0;
    }

    firebaseRemoteConfig.fetch(cacheExpiration)
            .addOnCompleteListener(getMvpView().getActivity(), task -> {
              if (task.isSuccessful()) {
                Log.d("VUTdebug", "Fetch success");
                firebaseRemoteConfig.activateFetched();
              } else {
                Log.d("VUTdebug", "Fetch failed");
              }

            });
  }


  private boolean isOnline() {
    try {
      CountDownLatch latch = new CountDownLatch(1);
      Thread t = new Thread() {
        @Override
        public void run() {
          try {
            int timeoutMs = 1500;
            Socket sock = new Socket();
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();
            online = true;
            latch.countDown();
          } catch (Exception e) {
            online = false;
            Log.e("VUTdebug", "Couldnt check net connection " + e.toString());
          }
        }
      };
      t.start();
      latch.await();
    } catch (Exception e) {
      online = false;
      Log.e("VUTdebug", "Couldnt check net connection " + e.toString());
    }
    return online;
  }

  private class LoginAsyncTask extends AsyncTask<Void, Integer, Boolean> {

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      loading = true;
    }

    protected Boolean doInBackground(Void... params) {
      boolean success;
      try {
        success = vutLogin.LogIn(prefs.getString(SharedPrefsEnums.username.toString(), ""), prefs.getString(SharedPrefsEnums.password.toString(), ""), eventBus, indexService);
      } catch (Exception e) {
        success = false;
        Log.e("VUTdebug", "Request error: " + e.toString());
      }

      return success;
    }

    protected void onProgressUpdate(Integer... progress) {

    }

    protected void onPostExecute(Boolean result) {
      if (result) {
        prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(SharedPrefsEnums.logedIn.toString(), true);
        editor.apply();

        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
          Bundle bundle = new Bundle();
          bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "Loged in!");
          firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
        }


        String startupModule = prefs.getString(SharedPrefsEnums.startupModule.toString(), "Dashboard");
        if (startupModule.matches(StartupModules.Dashboard.toString())) {
          Intent intent = new Intent(getMvpView().getActivity(), DashboardActivity.class);
          getMvpView().getActivity().startActivity(intent);
        } else if (startupModule.matches(StartupModules.Index.toString())) {
          Intent intent = new Intent(getMvpView().getActivity(), IndexActivity.class);
          getMvpView().getActivity().startActivity(intent);
        } else if (startupModule.matches(StartupModules.Intra.toString())) {
          Intent intent = new Intent(getMvpView().getActivity(), WebViewActivity.class);
          intent.putExtra(WebViewActivity.EXTRA_WEBVIEW_URL, "https://www.vutbr.cz/login/intra");
          getMvpView().getActivity().startActivity(intent);
        } else if (startupModule.matches(StartupModules.Studis.toString())) {
          Intent intent = new Intent(getMvpView().getActivity(), WebViewActivity.class);
          intent.putExtra(WebViewActivity.EXTRA_WEBVIEW_URL, "https://www.vutbr.cz/login/studis");
          getMvpView().getActivity().startActivity(intent);
        }
      } else {
        getMvpView().showToast(appCntx.getString(R.string.login_error));
      }
    }
  }

}