package prvnimilion.vutindex.mvp.presenters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.concurrent.CountDownLatch;

import javax.inject.Inject;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import prvnimilion.vutindex.BuildConfig;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.mvp.base.BasePresenter;
import prvnimilion.vutindex.mvp.views.MainActivityMvpView;
import prvnimilion.vutindex.pojo.SharedPrefsEnums;
import prvnimilion.vutindex.pojo.StartupModules;
import prvnimilion.vutindex.services.IndexService;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.tools.VutLogin;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;
import prvnimilion.vutindex.ui.activities.DashboardActivity;
import prvnimilion.vutindex.ui.activities.IndexActivity;
import prvnimilion.vutindex.ui.activities.WebViewActivity;

public class MainPresenter extends BasePresenter<MainActivityMvpView> {

  public ObservableField<String> appNameText = new ObservableField<>();
  public ObservableField<String> editTextName = new ObservableField<>();
  public ObservableField<String> editTextPwd = new ObservableField<>();
  public ObservableBoolean rememberCredentials = new ObservableBoolean();
  private Context appCntx;
  private ObservableLifeCycleManager observableLifeCycleManager;
  private Logger logger;
  private RxEventBus eventBus;
  private SharedPreferences prefs;
  private IndexService indexService;
  private VutLogin vutLogin;
  private DbOpenHelper dbOpenHelper;

  private FirebaseAnalytics firebaseAnalytics;

  private boolean loading = false;
  private boolean online = false;

  @Inject
  public MainPresenter(@ApplicationContext Context appContext,
                       ObservableLifeCycleManager observableLifeCycleManager,
                       Logger logger,
                       DbOpenHelper db,
                       RxEventBus eventBus,
                       IndexService indexService,
                       VutLogin vutLogin) {
    this.dbOpenHelper = db;
    this.appCntx = appContext;
    this.observableLifeCycleManager = observableLifeCycleManager;
    this.logger = logger;
    this.eventBus = eventBus;
    this.indexService = indexService;
    this.vutLogin = vutLogin;
  }

  @Override
  public void attachView(MainActivityMvpView mvpView, boolean isNewlyCreated) {
    super.attachView(mvpView, isNewlyCreated);
    init();
  }

  private void init() {
    appNameText.set(appCntx.getString(R.string.app_name));

    firebaseAnalytics = FirebaseAnalytics.getInstance(getMvpView().getActivity());

    prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);

    termsAndConditions();
    loadCredentials();
  }

  public void LogInButtonClick(View view) {
    if (isOnline()) {
      saveCredentials();
      new LoginAsyncTask().execute();
    } else {
      getMvpView().showToast(appCntx.getString(R.string.not_connected));
    }
  }

  public void PasswordRulesButtonClick(View view) {
    showTermsAndConditions();
  }

  private void saveCredentials() {
    prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(SharedPrefsEnums.username.toString(), editTextName.get());
    editor.putString(SharedPrefsEnums.password.toString(), editTextPwd.get());
    editor.putBoolean(SharedPrefsEnums.rememberCredentials.toString(), rememberCredentials.get());
    editor.apply();
  }

  private void loadCredentials() {
    prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);

    if (!prefs.getBoolean(SharedPrefsEnums.rememberCredentials.toString(), false))
      return;

    String username = prefs.getString(SharedPrefsEnums.username.toString(), "");
    String password = prefs.getString(SharedPrefsEnums.password.toString(), "");

    if ((username.matches("") || password.matches("")))
      return;

    editTextName.set(username);
    editTextPwd.set(password);
    rememberCredentials.set(prefs.getBoolean(SharedPrefsEnums.rememberCredentials.toString(), false));
  }

  public boolean bigShackMode(View view) {
    if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
      firebaseAnalytics.logEvent(FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT, null);
    }
    getMvpView().getIcon().setImageResource(R.drawable.bigshaq);
    appNameText.set(appCntx.getString(R.string.easter_egg_app_name));

    return false;
  }

  private void termsAndConditions() {
    final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appCntx);
    boolean agreed = sharedPreferences.getBoolean(SharedPrefsEnums.agreed.toString(), false);
    if (!agreed) {
      showRequiredTermsAndConditions();
    }
  }

  private void showRequiredTermsAndConditions() {
    final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appCntx);
    new AlertDialog.Builder(getMvpView().getActivity())
            .setTitle(appCntx.getString(R.string.password_rules))
            .setPositiveButton(appCntx.getString(R.string.yes), (dialog, which) -> {
              SharedPreferences.Editor editor = sharedPreferences.edit();
              editor.putBoolean(SharedPrefsEnums.agreed.toString(), true);
              editor.apply();
            })
            .setNegativeButton(appCntx.getString(R.string.no), (dialog, which) -> android.os.Process.killProcess(android.os.Process.myPid()))
            .setMessage(appCntx.getString(R.string.termsAndConditions))
            .show();
  }

  private void showTermsAndConditions() {
    new AlertDialog.Builder(getMvpView().getActivity())
            .setTitle(appCntx.getString(R.string.password_rules))
            .setPositiveButton(appCntx.getString(R.string.yes), (dialog, which) -> {
            })
            .setMessage(appCntx.getString(R.string.termsAndConditions))
            .show();
  }

  public void showRememberCredentialsWarning() {
    if (!rememberCredentials.get())
      return;

    new AlertDialog.Builder(getMvpView().getActivity())
            .setTitle(appCntx.getString(R.string.warning))
            .setPositiveButton(appCntx.getString(R.string.yes), (dialog, which) -> {

            })
            .setMessage(appCntx.getString(R.string.save_credentials_warning))
            .show();
  }

  private boolean isOnline() {
    try {
      CountDownLatch latch = new CountDownLatch(1);
      Thread t = new Thread() {
        @Override
        public void run() {
          try {
            int timeoutMs = 1500;
            Socket sock = new Socket();
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();
            online = true;
            latch.countDown();
          } catch (Exception e) {
            online = false;
            Log.e("VUTdebug", "Couldnt check net connection " + e.toString());
          }
        }
      };
      t.start();
      latch.await();
    } catch (Exception e) {
      online = false;
      Log.e("VUTdebug", "Couldnt check net connection " + e.toString());
    }
    return online;
  }

  private class LoginAsyncTask extends AsyncTask<Void, Integer, Boolean> {

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      loading = true;
      getMvpView().showLoadingPanel(true);
    }

    protected Boolean doInBackground(Void... params) {
      boolean success;
      try {
        success = vutLogin.LogIn(editTextName.get(), editTextPwd.get(), eventBus, indexService);
      } catch (Exception e) {
        success = false;
        Log.e("VUTdebug", "Request error: " + e.toString());
      }

      return success;
    }

    protected void onProgressUpdate(Integer... progress) {

    }

    protected void onPostExecute(Boolean result) {
      if (result) {
        prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(SharedPrefsEnums.logedIn.toString(), true);
        editor.apply();

        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
          Bundle bundle = new Bundle();
          bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "Loged in!");
          firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
        }


        String startupModule = prefs.getString(SharedPrefsEnums.startupModule.toString(), "Dashboard");
        if (startupModule.matches(StartupModules.Dashboard.toString())) {
          Intent intent = new Intent(getMvpView().getActivity(), DashboardActivity.class);
          getMvpView().getActivity().startActivity(intent);
        } else if (startupModule.matches(StartupModules.Index.toString())) {
          Intent intent = new Intent(getMvpView().getActivity(), IndexActivity.class);
          getMvpView().getActivity().startActivity(intent);
        } else if (startupModule.matches(StartupModules.Intra.toString())) {
          Intent intent = new Intent(getMvpView().getActivity(), WebViewActivity.class);
          intent.putExtra(WebViewActivity.EXTRA_WEBVIEW_URL, "https://www.vutbr.cz/login/intra");
          getMvpView().getActivity().startActivity(intent);
        } else if (startupModule.matches(StartupModules.Studis.toString())) {
          Intent intent = new Intent(getMvpView().getActivity(), WebViewActivity.class);
          intent.putExtra(WebViewActivity.EXTRA_WEBVIEW_URL, "https://www.vutbr.cz/login/studis");
          getMvpView().getActivity().startActivity(intent);
        }
      } else {
        getMvpView().showLoadingPanel(false);
        getMvpView().showToast(appCntx.getString(R.string.login_error));
      }
    }
  }

}