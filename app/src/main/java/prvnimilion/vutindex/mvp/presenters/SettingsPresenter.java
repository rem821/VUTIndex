package prvnimilion.vutindex.mvp.presenters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.SeekBar;

import com.google.firebase.analytics.FirebaseAnalytics;

import javax.inject.Inject;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import prvnimilion.vutindex.BuildConfig;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.mvp.base.BasePresenter;
import prvnimilion.vutindex.mvp.views.SettingsActivityMvpView;
import prvnimilion.vutindex.pojo.SharedPrefsEnums;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.tools.WorkerManager;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;
import prvnimilion.vutindex.ui.activities.MainActivity;

public class SettingsPresenter extends BasePresenter<SettingsActivityMvpView> {

  public ObservableBoolean isFalseAlarmChecked = new ObservableBoolean();
  public ObservableBoolean isSlowAlarmChecked = new ObservableBoolean();
  public ObservableField<String> seekBarCaption = new ObservableField();


  private Context appCntx;
  private ObservableLifeCycleManager observableLifeCycleManager;
  private Logger logger;
  private RxEventBus eventBus;
  private SharedPreferences prefs;
  private DbOpenHelper dbOpenHelper;

  private FirebaseAnalytics firebaseAnalytics;

  @Inject
  public SettingsPresenter(@ApplicationContext Context appContext,
                           ObservableLifeCycleManager observableLifeCycleManager,
                           Logger logger,
                           DbOpenHelper db,
                           RxEventBus eventBus) {
    this.dbOpenHelper = db;
    this.appCntx = appContext;
    this.observableLifeCycleManager = observableLifeCycleManager;
    this.logger = logger;
    this.eventBus = eventBus;
  }

  @Override
  public void attachView(SettingsActivityMvpView mvpView, boolean isNewlyCreated) {
    super.attachView(mvpView, isNewlyCreated);
    init();
  }

  private void init() {
    firebaseAnalytics = FirebaseAnalytics.getInstance(appCntx);
    prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
    initSwitch();

    getMvpView().getSeekbar().setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int seekProgress, boolean fromUser) {
        int progress = 15 + seekProgress * 5;
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(SharedPrefsEnums.syncTime.toString(), progress);
        editor.apply();

        setSeekbarCaption();
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {

      }
    });

  }

  public void falseAlarmCheckChanged() {
    SharedPreferences.Editor editor = prefs.edit();
    editor.putBoolean(SharedPrefsEnums.falseAlarm.toString(), isFalseAlarmChecked.get());
    editor.apply();
  }

  public void slowAlarmCheckChanged() {
    SharedPreferences.Editor editor = prefs.edit();
    editor.putBoolean(SharedPrefsEnums.slowAlarm.toString(), isSlowAlarmChecked.get());
    editor.apply();

    if (isSlowAlarmChecked.get()) {
      getMvpView().getSeekbar().setVisibility(View.VISIBLE);
      getMvpView().getSeekbarCaption().setVisibility(View.VISIBLE);
      setSeekbarCaption();

      if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "Slow alarm changed!");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, String.valueOf(isSlowAlarmChecked.get()));
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.TUTORIAL_COMPLETE, bundle);
      }
    } else {
      getMvpView().getSeekbar().setVisibility(View.GONE);
      getMvpView().getSeekbarCaption().setVisibility(View.GONE);
    }
  }

  private void initSwitch() {
    //isFalseAlarmChecked.set(prefs.getBoolean(SharedPrefsEnums.falseAlarm.toString(), false));
    isSlowAlarmChecked.set(prefs.getBoolean(SharedPrefsEnums.slowAlarm.toString(), false));

    setSeekbarCaption();
    int progress = (prefs.getInt(SharedPrefsEnums.syncTime.toString(), 15) - 15) / 5;
    getMvpView().getSeekbar().setProgress(progress);
    if (!isSlowAlarmChecked.get()) {
      getMvpView().getSeekbar().setVisibility(View.GONE);
      getMvpView().getSeekbarCaption().setVisibility(View.GONE);
    } else {
      getMvpView().getSeekbar().setVisibility(View.VISIBLE);
      getMvpView().getSeekbarCaption().setVisibility(View.VISIBLE);
    }
  }

  public void logOutClick(View view) {
    WorkerManager.stopService(appCntx);

    prefs = PreferenceManager.getDefaultSharedPreferences(appCntx);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putBoolean(SharedPrefsEnums.logedIn.toString(), false);
    editor.apply();

    Intent intent = new Intent(appCntx, MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    appCntx.startActivity(intent);
  }

  public void startupModuleClick(View view) {
    getMvpView().showStartupModuleDialog();
  }

  private void setSeekbarCaption() {
    seekBarCaption.set(String.format("%s %s %s", appCntx.getString(R.string.index_check_time_string), prefs.getInt(SharedPrefsEnums.syncTime.toString(), 15), appCntx.getString(R.string.minutes)));
  }

  public void onBackPressed() {
    WorkerManager.stopService(appCntx);
    if (prefs.getBoolean(SharedPrefsEnums.slowAlarm.toString(), false)) {
      WorkerManager.startService(appCntx, prefs.getInt(SharedPrefsEnums.syncTime.toString(), 15) * 60 * 1000);
    }
  }

}