package prvnimilion.vutindex.mvp.presenters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import prvnimilion.vutindex.BuildConfig;
import prvnimilion.vutindex.R;
import prvnimilion.vutindex.data.db.DbOpenHelper;
import prvnimilion.vutindex.di.ApplicationContext;
import prvnimilion.vutindex.mvp.base.BasePresenter;
import prvnimilion.vutindex.mvp.models.IndexTableView.IndexTableViewAdapter;
import prvnimilion.vutindex.mvp.models.IndexTableView.TableViewListener;
import prvnimilion.vutindex.mvp.models.IndexTableView.TableViewModel;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.Cell;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.ColumnHeader;
import prvnimilion.vutindex.mvp.models.IndexTableView.model.RowHeader;
import prvnimilion.vutindex.mvp.views.IndexActivityMvpView;
import prvnimilion.vutindex.pojo.Index;
import prvnimilion.vutindex.pojo.Semester;
import prvnimilion.vutindex.pojo.Subject;
import prvnimilion.vutindex.pojo.messages.IndexChangeMessage;
import prvnimilion.vutindex.services.IndexService;
import prvnimilion.vutindex.tools.Logger;
import prvnimilion.vutindex.tools.RxEventBus;
import prvnimilion.vutindex.tools.VutLogin;
import prvnimilion.vutindex.ui.ObservableLifeCycleManager;

public class IndexPresenter extends BasePresenter<IndexActivityMvpView> {

  private Context appCntx;
  private ObservableLifeCycleManager observableLifeCycleManager;
  private Logger logger;
  private RxEventBus eventBus;
  private IndexService indexService;
  private SharedPreferences prefs;
  private DbOpenHelper dbOpenHelper;
  private VutLogin vutLogin;

  private AbstractTableAdapter tableViewAdapter;
  private TableViewModel tableViewModel;

  private Index index;
  private List<Semester> semesters;
  private FirebaseRemoteConfig firebaseRemoteConfig;
  private boolean expanded = false;

  @Inject
  public IndexPresenter(@ApplicationContext Context appContext,
                        ObservableLifeCycleManager observableLifeCycleManager,
                        Logger logger,
                        DbOpenHelper db,
                        RxEventBus eventBus,
                        IndexService indexService,
                        VutLogin vutLogin) {
    this.dbOpenHelper = db;
    this.appCntx = appContext;
    this.observableLifeCycleManager = observableLifeCycleManager;
    this.logger = logger;
    this.eventBus = eventBus;
    this.indexService = indexService;
    this.vutLogin = vutLogin;
  }

  @Override
  public void attachView(IndexActivityMvpView mvpView, boolean isNewlyCreated) {
    super.attachView(mvpView, isNewlyCreated);
    init();
  }

  private void init() {
    firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    initEventBus();
    initializeTableView(getSemesterItems(), false);
  }

  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_index_action_reload:
        reloadTable(item);
        return true;
      case R.id.menu_index_action_expand:
        expandTable(item);
        return true;
      default:
        return false;
    }

  }

  private void expandTable(MenuItem item) {
    if (expanded) {
      expanded = false;
      item.setIcon(R.drawable.ic_expand_more_white_24dp);
      initializeTableView(semesters, false);
      getMvpView().toggleImmersive(false);
    } else {
      expanded = true;
      item.setIcon(R.drawable.ic_expand_less_white_24dp);
      initializeTableView(semesters, true);
      getMvpView().toggleImmersive(true);
    }
  }

  private void reloadTable(MenuItem item) {
    new ReloadAsyncTask().execute();
  }

  private List<Semester> getSemesterItems() {
    try {
      index = indexService.getIndexFromDb();
      semesters = index.getSemesters();
      return semesters;
    } catch (Exception e) {
      Log.e("VUTdebug", "Index load to screen error" + e.toString());
    }
    return null;
  }


  private void initEventBus() {
    this.observableLifeCycleManager.addObservableAndStartObserve("IndexChangeMessage", eventBus.filteredObservable(IndexChangeMessage.class), new Observer<IndexChangeMessage>() {
      @Override
      public void onSubscribe(Disposable d) {

      }

      @Override
      public void onNext(IndexChangeMessage value) {
        initializeTableView(getSemesterItems(), false);
      }

      @Override
      public void onError(Throwable e) {

      }

      @Override
      public void onComplete() {

      }
    }, true, AndroidSchedulers.mainThread());
  }

  @SuppressWarnings("unchecked")
  private void initializeTableView(List<Semester> semesters, boolean moreInfo) {
    if (semesters == null || semesters.size() == 0) {
      getMvpView().showToast(appCntx.getString(R.string.report_to_developer));
      if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("release")) {
        Crashlytics.log(Log.ERROR, "semesters == null", "initialize tableView semesters == null");
      }
      return;
    }

    List<Semester> currentSemesters = new ArrayList<>();
    if (!moreInfo) {
      Calendar cal = Calendar.getInstance();
      int cMonth = cal.get(Calendar.MONTH);
      if (cMonth < 1 || cMonth > 7) {
        currentSemesters.add(semesters.get(0));
      } else {
        currentSemesters.add(semesters.get(1));
      }
    } else {
      currentSemesters = semesters;
    }

    tableViewModel = new TableViewModel(appCntx);
    tableViewAdapter = new IndexTableViewAdapter(appCntx, tableViewModel);
    getMvpView().getTableView().setAdapter(tableViewAdapter);
    getMvpView().getTableView().setTableViewListener(new TableViewListener(getMvpView().getTableView()));

    List<String> columnHeaders = new ArrayList<String>() {{
      add(appCntx.getString(R.string.full_name));
      add(appCntx.getString(R.string.type));
      add(appCntx.getString(R.string.credits));
      add(appCntx.getString(R.string.completion));
      add(appCntx.getString(R.string.zapocet));
      add(appCntx.getString(R.string.points));
      add(appCntx.getString(R.string.grade));
      add(appCntx.getString(R.string.term));
      add(appCntx.getString(R.string.complete));
      add(appCntx.getString(R.string.vsp));

    }};
    List<ColumnHeader> columnHeaderItems = new ArrayList<>();
    List<RowHeader> rowHeaderItems = new ArrayList<>();
    List<List<Cell>> cellItems = new ArrayList<>();
    List<Cell> blankCells = new ArrayList<>();

    int j = 0;
    for (String columnHeader : columnHeaders) {
      columnHeaderItems.add(new ColumnHeader(String.valueOf(j), columnHeader));
      blankCells.add(new Cell(String.valueOf(j), ""));
      j++;
    }

    int i = 0;
    for (Semester semester : currentSemesters) {
      i++;
      for (Subject subject : semester.getSubjects()) {
        List<Cell> cells = new ArrayList<>();
        RowHeader rowHeader = new RowHeader(String.valueOf(i), subject.getSubjectShortName());
        cells.add(new Cell("0", subject.getSubjectFullName()));
        cells.add(new Cell("1", subject.getType()));
        cells.add(new Cell("2", subject.getCredits()));
        cells.add(new Cell("3", subject.getCompletion()));
        cells.add(new Cell("4", subject.isCreditGiven()));
        cells.add(new Cell("5", subject.getPoints()));
        cells.add(new Cell("6", subject.getGrade()));
        cells.add(new Cell("7", subject.getTermTime()));
        cells.add(new Cell("8", subject.isPassed()));
        cells.add(new Cell("9", subject.getVsp()));
        rowHeaderItems.add(rowHeader);
        cellItems.add(cells);
        i++;
      }
    }
    tableViewAdapter.setAllItems(columnHeaderItems, rowHeaderItems, cellItems);
  }

  private class ReloadAsyncTask extends AsyncTask<Void, Integer, Boolean> {

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      getMvpView().showLoadingPanel(true);
    }

    protected Boolean doInBackground(Void... params) {
      try {
        vutLogin.refreshData(index);
      } catch (Exception e) {
        Log.d("VUTdebug", "Couldnt refresh index: " + e);
      }
      return true;
    }

    protected void onProgressUpdate(Integer... progress) {

    }

    protected void onPostExecute(Boolean result) {
      super.onPostExecute(result);

      initializeTableView(getSemesterItems(), false);

      final Handler handler = new Handler();
      handler.postDelayed(() -> {
        getMvpView().showLoadingPanel(false);
      }, 1000);
    }
  }
}