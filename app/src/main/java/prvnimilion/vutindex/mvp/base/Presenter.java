package prvnimilion.vutindex.mvp.base;

/**
 * Created by stand on 12.08.2017.
 */

public interface Presenter<V extends MvpView> {
  void attachView(V mvpView, boolean isNewlyCreated);

  void detachView();
}