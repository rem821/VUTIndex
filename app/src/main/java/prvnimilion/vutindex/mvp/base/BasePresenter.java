package prvnimilion.vutindex.mvp.base;

/**
 * Created by stand on 12.08.2017.
 */

public abstract class BasePresenter<T extends MvpView> implements Presenter<T> {

  private T mMvpView;

  @Override
  public void attachView(T mvpView, boolean isNewlyCreated) {
    mMvpView = mvpView;
  }

  @Override
  public void detachView() {
    mMvpView = null;
  }

  public boolean isViewAttached() {
    return mMvpView != null;
  }

  protected T getMvpView() {
    return mMvpView;
  }

  protected void checkViewAttached() {
    if (!isViewAttached()) throw new MvpViewNotAttachedException();
  }

  public static class MvpViewNotAttachedException extends RuntimeException {
    public MvpViewNotAttachedException() {
      super("Please call Presenter.attachView(MvpView) before" +
              " requesting data to the Presenter");
    }
  }
}